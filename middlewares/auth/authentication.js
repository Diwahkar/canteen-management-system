const { users, sequelize } = require("../../models");

const jwtDecode = require("jwt-decode");
const jwt = require("jsonwebtoken");
const usersRepos = require("../../repos/users.repos");
const ErrorClass = require("../../utils/custom.error");
const jwtSecretsRepos = require("../../repos/jwtSecrets.repos");

module.exports = async (req, res, next) => {
  try {
    const response = await sequelize.transaction(async (t) => {
      const token = req.header("Authorization")?.split(" ")[1];
      const unVerifiedPayload = jwtDecode(token);
      const jwtSecret = await jwtSecretsRepos.findOne(
        { where: { username: unVerifiedPayload.sub } },
        t
      );
      if (!jwtSecret) {
        throw new ErrorClass(
          401,
          false,
          "authentication",
          "Access Denied: Pending Sign Up Or Email Verification"
        );
      }
      const verifiedPayload = jwt.verify(token, jwtSecret.key);
      req.user = await usersRepos.findOne(
        { where: { username: verifiedPayload.sub } },
        t
      );
      return next();
    });
    return response;
  } catch (error) {
    error.origin = "authentication";
    return next(error);
  }
};

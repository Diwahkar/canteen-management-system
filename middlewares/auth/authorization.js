const {
  accesses,
  modules,
  privileges,
  routes,
  userRoles,
  sequelize,
} = require("../../models");
const accessesRepos = require("../../repos/accesses.repos");
const ErrorClass = require("../../utils/custom.error");

module.exports = async (req, res, next) => {
  try {
    const response = sequelize.transaction(async (t) => {
      let moduleCode, privilegeCode;
      let isAuthorized = false;

      const method = req.method;
      const urlFormat = req.route.path;
      const route = await routes.findOne({
        where: {
          method,
          url: urlFormat,
        },
      });
      if (route?.privilegeCode) privilegeCode = route.privilegeCode;
      else
        return next(
          new ErrorClass(401, false, "authorization", "Unauthorized")
        );

      moduleCode = urlFormat.split("/")[2];
      const user = req.user;
      const allRoles = await userRoles.findAll({
        where: {
          username: user.username,
        },
      });
      if (!allRoles.length)
        return next(
          new ErrorClass(401, false, "authorization", "Unauthorized")
        );

      const allRoleCodes = allRoles.map((role) => role.roleCode);
      const access = await accessesRepos.findOne({
        where: {
          roleCode: allRoleCodes,
          moduleCode,
          privilegeCode,
        },
      });
      if (access?.id) isAuthorized = true;
      if (isAuthorized) return next();
      return next(new ErrorClass(401, false, "authorization", "Unauthorized"));
    });
    return response;
  } catch (error) {
    error.origin = "authorization";
    return next(error);
  }
};

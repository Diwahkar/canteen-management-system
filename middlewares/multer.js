const multer = require("multer");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const fieldName = file.fieldname;
    const path = `./public/${fieldName}`;
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename: (req, file, cb) => {
    const ext = file.mimetype.split("/")[1];
    const filename = file.originalname
      .split(".")[0]
      .trim()
      .replaceAll(" ", "_");
    cb(null, `${filename}.${ext}`);
  },
});

const multerFilter = (req, file, cb) => {
  if (["jpeg", "png"].some((ext) => ext === file.mimetype.split("/")[1])) {
    cb(null, true);
  } else {
    cb(new Error("File extension Not Allowed"), false);
  }
};

const upload = multer({ storage, fileFilter: multerFilter });

module.exports = {
  userImage: (req, res, next) => {
    upload.single("userImage")(req, res, (error) => {
      if (error) {
        (error.statusCode = 422),
          (error.success = false),
          (error.origin = "multer (userImage)");
        return next(error);
      }
      req.body.displayPic = req.file?.path;
      next();
    });
  },

  itemImage: (req, res, next) => {
    upload.single("itemImage")(req, res, (error) => {
      if (error) {
        (error.statusCode = 422),
          (error.success = false),
          (error.origin = "multer (itemImage)");
        return next(error);
      }
      req.body.image = req.file?.path;
      next();
    });
  },
};

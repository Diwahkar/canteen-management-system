const Joi = require("joi");

const registerSchema = Joi.object({
  username: Joi.string().required(),
  deposit: Joi.number().required(),
  password: Joi.string().required(),
});

module.exports = {
  registerValidation: (req, res, next) =>
    validate(req, res, next, registerSchema),
};

function validate(req, res, next, schema) {
  const { value, error } = schema.validate(req.body);
  if (error) {
    (error.statusCode = 422),
      (error.success = false),
      (error.origin = error.name || "Payment Validation");
    next(error);
  } else {
    req.body = value;
    next();
  }
}

const Joi = require("joi");

const registerSchema = Joi.object({
  name: Joi.string().min(3).max(255).required(),
  password: Joi.string(),
});

const updateSchema = Joi.object({
  name: Joi.string(),
  password: Joi.string(),
});

const deleteSchema = Joi.object({
  password: Joi.string().required(),
});

module.exports = {
  registerValidation: (req, res, next) =>
    validate(req, res, next, registerSchema),
  updateValidation: (req, res, next) => validate(req, res, next, updateSchema),
  deleteValidation: (req, res, next) => validate(req, res, next, deleteSchema),
};

function validate(req, res, next, schema) {
  const { value, error } = schema.validate(req.body);
  if (error) {
    (error.statusCode = 422),
      (error.success = false),
      (error.origin = error.name || "Category Validation");
    next(error);
  } else {
    req.body = value;
    next();
  }
}

const Joi = require("joi");

const registerSchema = Joi.object({
  name: Joi.string().min(3).max(255).required(),
  price: Joi.number().required(),
  image: Joi.string(),
  availableQuantity: Joi.number().default(1),
  categoryName: Joi.string(),
  password: Joi.string().required(),
});

const updateSchema = Joi.object({
  name: Joi.string().min(3).max(255),
  price: Joi.number(),
  availableQuantity: Joi.number(),
  image: Joi.string(),
  availableQuantity: Joi.number(),
  categoryName: Joi.string(),
  password: Joi.string().required(),
});

const deleteSchema = Joi.object({
  password: Joi.string().required(),
});

module.exports = {
  registerValidation: (req, res, next) =>
    validate(req, res, next, registerSchema),
  updateValidation: (req, res, next) => validate(req, res, next, updateSchema),
  deleteValidation: (req, res, next) => validate(req, res, next, deleteSchema),
};

function validate(req, res, next, schema) {
  const { value, error } = schema.validate(req.body);
  if (error) {
    (error.statusCode = 422),
      (error.success = false),
      (error.origin = error.name || "Item Validation");
    next(error);
  } else {
    req.body = value;
    next();
  }
}

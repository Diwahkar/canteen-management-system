const Joi = require("joi");

const registerSchema = Joi.object({
  roleCode: Joi.string().required(),
  moduleCode: Joi.string().required(),
  privilegeCode: Joi.string().required(),
  password: Joi.string().required(),
});

const updateSchema = Joi.object({
  roleCode: Joi.string(),
  moduleCode: Joi.string(),
  privilegeCode: Joi.string(),
  password: Joi.string().required(),
});

const deleteSchema = Joi.object({
  password: Joi.string().required(),
});

module.exports = {
  registerValidation: (req, res, next) =>
    validate(req, res, next, registerSchema),
  updateValidation: (req, res, next) => validate(req, res, next, updateSchema),
  deleteValidation: (req, res, next) => validate(req, res, next, deleteSchema),
};

function validate(req, res, next, schema) {
  const { value, error } = schema.validate(req.body);
  if (error) {
    (error.statusCode = 422),
      (error.success = false),
      (error.origin = error.name || "Access Validation");
    next(error);
  } else {
    req.body = value;
    next();
  }
}

const Joi = require("joi");
const passwordComplexity = require("joi-password-complexity");

const registerSchema = Joi.object({
  name: Joi.string().min(5).max(255).required(),
  username: Joi.string().alphanum().min(5).max(255).required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net", "org"] } })
    .required(),
  password: passwordComplexity().required(),
  confirmPassword: Joi.valid(Joi.ref("password")).required(),
  pin: Joi.string().length(4).pattern(/^\d+$/).required(),
  department: Joi.string().required(),
  displayPic: Joi.string().required(),
});

emailVerifySchema = Joi.object({
  usernameOrEmail: Joi.string().required(),
  emailVerifyCode: Joi.string().required(),
});

const loginSchema = Joi.object({
  usernameOrEmail: Joi.string().required(),
  password: Joi.string().required(),
});

const forgotPasswordSchema = Joi.object({
  usernameOrEmail: Joi.string().required(),
});

const resetForgottenPasswordSchema = Joi.object({
  usernameOrEmail: Joi.string().required(),
  resetForgottenPasswordCode: Joi.string().required(),
  newPassword: passwordComplexity().required(),
  confirmNewPassword: Joi.valid(Joi.ref("newPassword")).required(),
});

const updateSchema = Joi.object({
  name: Joi.string().min(5).max(255),
  username: Joi.string().alphanum().min(5).max(255),
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net", "org"] },
  }),
  newPassword: passwordComplexity(),
  confirmNewPassword: Joi.valid(Joi.ref("newPassword")),
  pin: Joi.string().length(4).pattern(/^\d+$/),
  displayPic: Joi.string(),
  password: Joi.string().required(),
}).with("newPassword", "confirmNewPassword");

const updateByAdminSchema = Joi.object({
  position: Joi.string().required(),
  password: Joi.string().required(),
});

const deleteSchema = Joi.object({
  password: Joi.string().required(),
});

module.exports = {
  registerValidation: (req, res, next) =>
    validate(req, res, next, registerSchema),
  emailVerifyValidation: (req, res, next) =>
    validate(req, res, next, emailVerifySchema),
  loginValidation: (req, res, next) => validate(req, res, next, loginSchema),
  forgotPasswordValidation: (req, res, next) =>
    validate(req, res, next, forgotPasswordSchema),
  resetForgottenPasswordValidation: (req, res, next) =>
    validate(req, res, next, resetForgottenPasswordSchema),
  updateValidation: (req, res, next) => validate(req, res, next, updateSchema),
  updateByAdminValidation: (req, res, next) =>
    validate(req, res, next, updateByAdminSchema),
  deleteValidation: (req, res, next) => validate(req, res, next, deleteSchema),
};

function validate(req, res, next, schema) {
  let { value, error } = schema.validate(req.body);
  if (error) {
    (error.status = 422),
      (error.success = false),
      (error.name = error.name || "User Validation");
    next(error);
  } else {
    req.body = value;
    next();
  }
}

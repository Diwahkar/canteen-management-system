const Joi = require("joi");
const findRoles = require("../../utils/find.roles");

const registerSchema = Joi.object({
  orderedItemsArr: Joi.array()
    .items(
      Joi.object({
        itemName: Joi.string().required(),
        quantity: Joi.number().required(),
      }).required()
    )
    .required(),
  pin: Joi.string().required(),
});

const updateSchema = Joi.object({
  orderedItemsArr: Joi.array().items(
    Joi.object({
      itemName: Joi.string().required(),
      quantity: Joi.number().required(),
    }).required()
  ),
  pin: Joi.string().required(),
});

const deleteSchema = Joi.object({
  password: Joi.string().required(),
});

module.exports = {
  registerValidation: (req, res, next) =>
    validate(req, res, next, registerSchema),
  updateValidation: async (req, res, next) =>
    validate(req, res, next, updateSchema),
  deleteValidation: (req, res, next) => validate(req, res, next, deleteSchema),
};

function validate(req, res, next, schema) {
  const { value, error } = schema.validate(req.body);
  if (error) {
    (error.statusCode = 422),
      (error.success = false),
      (error.origin = error.name || "Order Validation");
    return next(error);
  } else {
    req.body = value;
    return next();
  }
}

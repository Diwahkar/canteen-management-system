const Joi = require("joi");

const registerSchema = Joi.object({
  orderId: Joi.number().required(),
  amountReceived: Joi.number().default(0),
  amountReturned: Joi.number().default(0),
  password: Joi.string().required(),
  // remarks: Joi.string().valid('surplus', 'balance', 'deficit')
});

// const updateSchema = Joi.object({
//   ready: Joi.boolean(),
// })

module.exports = {
  registerValidation: (req, res, next) =>
    validate(req, res, next, registerSchema),
  // updateValidation: (req, res, next) => validate(req, res, next, updateSchema),
};

function validate(req, res, next, schema) {
  const { value, error } = schema.validate(req.body);
  if (error) {
    (error.statusCode = 422),
      (error.success = false),
      (error.origin = error.name || "Payment Validation");
    next(error);
  } else {
    req.body = value;
    next();
  }
}

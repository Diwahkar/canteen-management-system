module.exports = (req, res, next) => {
  const error = new Error("Route Not Found");
  error.statusCode = 404;
  error.success = false;
  error.origin = "Unknown Route";
  next(error);
};

module.exports = (error, req, res, next) => {
  res.status(error.status || 500).json({
    status: error.status || 500,
    success: error.success || false,
    origin: error.name || "Unknown",
    message: error.message || "Something Went Wrong",
  });
};

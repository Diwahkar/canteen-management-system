"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("users", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      username: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      email: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      pin: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      department: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      position: {
        allowNull: false,
        defaultValue: "junior",
        type: Sequelize.ENUM,
        values: ["senior", "mid", "junior"],
      },
      displayPic: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      emailVerified: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      emailVerifyCode: {
        defaultValue: null,
        type: Sequelize.STRING,
      },
      forgotPassword: {
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      resetForgottenPasswordCode: {
        defaultValue: null,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("users");
  },
};

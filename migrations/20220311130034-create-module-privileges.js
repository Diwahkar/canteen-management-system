"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("modulePrivileges", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      moduleCode: {
        allowNull: false,
        type: Sequelize.STRING,
        references: {
          model: "modules",
          key: "code",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      privilegeCode: {
        allowNull: false,
        type: Sequelize.STRING,
        references: {
          model: "privileges",
          key: "code",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("modulePrivileges");
  },
};

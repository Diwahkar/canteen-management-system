"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addConstraint("orders", {
      fields: ["paymentId"],
      type: "foreign key",
      name: "orders_paymentId_fkey", // optional
      references: {
        table: "payments",
        field: "id", // it must be field not key it seems
      },
      onDelete: "CASCADE",
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeConstraint("orders", "orders_paymentId_fkey");
  },
};

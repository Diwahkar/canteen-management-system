onUpdate: "CASCADE", "use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addConstraint("payments", {
      fields: ["transactionId"],
      type: "foreign key",
      name: "payments_transactionId_fkey", // optional
      references: {
        table: "transactions",
        field: "id", // it must be field not key it seems
      },
      onDelete: "CASCADE",
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeConstraint(
      "payments",
      "payments_transactionId_fkey"
    );
  },
};

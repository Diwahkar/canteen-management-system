const fs = require("fs");
const path = require("path");
module.exports = (app) => {
  const contents = fs.readdirSync(__dirname);
  contents.forEach((content) => {
    if (fs.lstatSync(path.join(__dirname, `${content}`)).isFile()) {
      if (content !== "index.js") require(`./${content}`)(app);
    } else {
      const files = fs.readdirSync(path.join(__dirname, `${content}`));
      files.forEach((file) => require(`./${content}` + `/${file}`)(app));
    }
  });
};

const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/modulePrivileges.validations");
const modulePrivilegeControllers = require("../../controllers/modulePrivileges.controllers");

module.exports = (app) => {
  app
    .route("/api/modulePrivileges")
    .post(
      authentication,
      authorization,
      registerValidation,
      modulePrivilegeControllers.registerModulePrivilege
    )
    .get(
      authentication,
      authorization,
      modulePrivilegeControllers.getAllModulePrivileges
    );

  app
    .route("/api/modulePrivileges/:id")
    .get(
      authentication,
      authorization,
      modulePrivilegeControllers.getModulePrivilegeById
    )
    .put(
      authentication,
      authorization,
      updateValidation,
      modulePrivilegeControllers.updateModulePrivilegeById
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      modulePrivilegeControllers.deleteModulePrivilegeById
    );

  app
    .route("/api/modulePrivileges/module/:moduleCode")
    .get(
      authentication,
      authorization,
      modulePrivilegeControllers.getAllModulePrivilegesByModule
    );

  app
    .route("/api/modulePrivileges/privilege/:privilegeCode")
    .get(
      authentication,
      authorization,
      modulePrivilegeControllers.getAllModulePrivilegesByPrivilege
    );
};

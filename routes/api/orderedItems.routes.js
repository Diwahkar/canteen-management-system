const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const orderedItemsControllers = require("../../controllers/orderedItems.controllers");

module.exports = (app) => {
  app
    .route("/api/orderedItems")
    .get(
      authentication,
      authorization,
      orderedItemsControllers.getAllOrderedItems
    );

  app
    .route("/api/orderedItems/:id")
    .get(
      authentication,
      authorization,
      orderedItemsControllers.getOrderedItemById
    );

  app
    .route("/api/orderedItems/orderId/:orderId")
    .get(
      authentication,
      authorization,
      orderedItemsControllers.getAllOrderedItemsByOrderId
    );
};

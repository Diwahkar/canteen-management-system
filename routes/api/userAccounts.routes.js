const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const userAccountsControllers = require("../../controllers/userAccounts.controllers");

module.exports = (app) => {
  app
    .route("/api/userAccounts")
    .get(
      authentication,
      authorization,
      userAccountsControllers.getAllUserAccounts
    );

  app
    .route("/api/userAccounts/:username")
    .get(
      authentication,
      authorization,
      userAccountsControllers.getUserAccountByUsername
    );
};

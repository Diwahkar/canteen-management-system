const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/orders.validations");
const ordersControllers = require("../../controllers/orders.controllers");

module.exports = (app) => {
  app
    .route("/api/orders")
    .post(
      authentication,
      authorization,
      registerValidation,
      ordersControllers.registerOrder
    )
    .get(authentication, authorization, ordersControllers.getAllOrders);

  app
    .route("/api/orders/:id")
    .get(authentication, authorization, ordersControllers.getOrderById)
    .put(
      authentication,
      authorization,
      updateValidation,
      ordersControllers.updateOrderById
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      ordersControllers.deleteOrderById
    );

  app
    .route("/api/orders/user/:username")
    .get(
      authentication,
      authorization,
      ordersControllers.getAllOrdersByUsername
    );

  app
    .route("/api/orders/status/:id&:status")
    .put(
      authentication,
      authorization,
      ordersControllers.updateOrderStatusById
    );
};

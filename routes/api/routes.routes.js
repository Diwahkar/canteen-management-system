const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/routes.validations");
const routesControllers = require("../../controllers/routes.controllers");

module.exports = (app) => {
  app
    .route("/api/routes")
    .post(
      authentication,
      authorization,
      registerValidation,
      routesControllers.registerRoute
    )
    .get(authentication, authorization, routesControllers.getAllRoutes);

  app
    .route("/api/routes/:id")
    .get(authentication, authorization, routesControllers.getRouteById)
    .put(
      authentication,
      authorization,
      updateValidation,
      routesControllers.updateRouteById
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      routesControllers.deleteRouteById
    );

  app
    .route("/api/routes/privilege/:privilegeCode")
    .get(
      authentication,
      authorization,
      routesControllers.getAllRoutesByPrivilege
    );
};

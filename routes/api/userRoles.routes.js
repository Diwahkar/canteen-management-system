const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/userRoles.validations");
const userRolesControllers = require("../../controllers/userRoles.controllers");

module.exports = (app) => {
  app
    .route("/api/userRoles")
    .post(
      authentication,
      authorization,
      registerValidation,
      userRolesControllers.registerUserRole
    )
    .get(authentication, authorization, userRolesControllers.getAllUserRoles);

  app
    .route("/api/userRoles/:id")
    .get(authentication, authorization, userRolesControllers.getUserRoleById)
    .put(
      authentication,
      authorization,
      updateValidation,
      userRolesControllers.updateUserRoleById
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      userRolesControllers.deleteUserRoleById
    );

  app
    .route("/api/userRoles/user/:username")
    .get(
      authentication,
      authorization,
      userRolesControllers.getAllUserRolesByUsername
    );
};

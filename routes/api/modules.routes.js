const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/modules.validations");
const moduledControllers = require("../../controllers/modules.controllers");

module.exports = (app) => {
  app
    .route("/api/modules")
    .post(
      authentication,
      authorization,
      registerValidation,
      moduledControllers.registerModule
    )
    .get(authentication, authorization, moduledControllers.getAllModules);

  app
    .route("/api/modules/:code")
    .get(authentication, authorization, moduledControllers.getModuleByCode)
    .put(
      authentication,
      authorization,
      updateValidation,
      moduledControllers.updateModuleByCode
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      moduledControllers.deleteModuleByCode
    );
};

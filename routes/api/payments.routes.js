const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
} = require("../../middlewares/validations/payments.validations");
const paymentsControllers = require("../../controllers/payments.controllers");

module.exports = (app) => {
  app
    .route("/api/payments")
    .post(
      authentication,
      authorization,
      registerValidation,
      paymentsControllers.registerPayment
    )
    .get(authentication, authorization, paymentsControllers.getAllPayments);

  app
    .route("/api/payments/:id")
    .get(authentication, authorization, paymentsControllers.getPaymentById);

  app
    .route("/api/payments/user/:username")
    .get(
      authentication,
      authorization,
      paymentsControllers.getAllPaymentsByUsername
    );
};

const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/categories.validations");
const categoriesControllers = require("../../controllers/categories.controllers");

module.exports = (app) => {
  app
    .route("/api/categories")
    .post(
      authentication,
      authorization,
      registerValidation,
      categoriesControllers.registerCategory
    )
    .get(authentication, authorization, categoriesControllers.getAllCategories);

  app
    .route("/api/categories/:name")
    .get(authentication, authorization, categoriesControllers.getCategoryByName)
    .put(
      authentication,
      authorization,
      updateValidation,
      categoriesControllers.updateCategoryByName
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      categoriesControllers.deleteCategoryByName
    );
};

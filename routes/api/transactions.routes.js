const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
} = require("../../middlewares/validations/transactions.validations");
const transactionsControllers = require("../../controllers/transactions.controllers");

module.exports = (app) => {
  app
    .route("/api/transactions")
    .post(
      authentication,
      authorization,
      registerValidation,
      transactionsControllers.registerTransaction
    )
    .get(
      authentication,
      authorization,
      transactionsControllers.getAllTransactions
    );

  app
    .route("/api/transactions/:id")
    .get(
      authentication,
      authorization,
      transactionsControllers.getTransactionById
    );

  app
    .route("/api/transactions/user/:username")
    .get(
      authentication,
      authorization,
      transactionsControllers.getAllTransactionsByUsername
    );
};

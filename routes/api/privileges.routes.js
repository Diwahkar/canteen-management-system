const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/privileges.validations");
const privilegesControllers = require("../../controllers/privileges.controllers");

module.exports = (app) => {
  app
    .route("/api/privileges")
    .post(
      authentication,
      authorization,
      registerValidation,
      privilegesControllers.registerPrivilege
    )
    .get(authentication, authorization, privilegesControllers.getAllPrivileges);

  app
    .route("/api/privileges/:code")
    .get(
      authentication,
      authorization,
      privilegesControllers.getPrivilegeByCode
    )
    .put(
      authentication,
      authorization,
      updateValidation,
      privilegesControllers.updatePrivilegeByCode
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      privilegesControllers.deletePrivilegeByCode
    );
};

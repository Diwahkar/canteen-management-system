const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/accesses.validations");
const accessesControllers = require("../../controllers/accesses.controllers");

module.exports = (app) => {
  app
    .route("/api/accesses")
    .post(
      authentication,
      authorization,
      registerValidation,
      accessesControllers.registerAccess
    )
    .get(authentication, authorization, accessesControllers.getAllAccesses);

  app
    .route("/api/accesses/:id")
    .get(authentication, authorization, accessesControllers.getAccessById)
    .put(
      authentication,
      authorization,
      updateValidation,
      accessesControllers.updateAccessById
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      accessesControllers.deleteAccessById
    );

  app
    .route("/api/accesses/role/:roleCode")
    .get(
      authentication,
      authorization,
      accessesControllers.getAllAccessesByRole
    );

  app
    .route("/api/accesses/module/:moduleCode")
    .get(
      authentication,
      authorization,
      accessesControllers.getAllAccessesByModule
    );

  app
    .route("/api/accesses/privilege/:privilegeCode")
    .get(
      authentication,
      authorization,
      accessesControllers.getAllAccessesByPrivilege
    );
};

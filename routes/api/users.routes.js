const { userImage } = require("../../middlewares/multer");
const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  emailVerifyValidation,
  loginValidation,
  forgotPasswordValidation,
  resetForgottenPasswordValidation,
  updateValidation,
  updateByAdminValidation,
  deleteValidation,
} = require("../../middlewares/validations/users.validations");
const usersControllers = require("../../controllers/users.controllers");

module.exports = (app) => {
  app
    .route("/api/users")
    .post(userImage, registerValidation, usersControllers.registerUser)
    .get(authentication, authorization, usersControllers.getAllUsers)
    .put(
      authentication,
      authorization,
      userImage,
      updateValidation,
      usersControllers.updateMyProfile
    )
    .delete(
      authentication,
      authentication,
      deleteValidation,
      usersControllers.deleteMyProfile
    );

  app
    .route("/api/users/email-verify")
    .post(emailVerifyValidation, usersControllers.emailVerify);

  app
    .route("/api/users/login")
    .post(loginValidation, usersControllers.loginUser);

  app
    .route("/api/users/forgot-password")
    .post(forgotPasswordValidation, usersControllers.forgotPassword);

  app
    .route("/api/users/reset-forgotten-password")
    .post(
      resetForgottenPasswordValidation,
      usersControllers.resetForgottenPassword
    );

  app
    .route("/api/users/:username")
    .get(authentication, authorization, usersControllers.getUserByUsername)
    .put(
      authentication,
      authorization,
      updateByAdminValidation,
      usersControllers.updateUserByUsername
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      usersControllers.deleteUserByUsername
    );
};

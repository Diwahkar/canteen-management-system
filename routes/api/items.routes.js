const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const { itemImage } = require("../../middlewares/multer");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/items.validations");
const itemsControllers = require("../../controllers/items.controllers");

module.exports = (app) => {
  app
    .route("/api/items")
    .post(
      authentication,
      authorization,
      itemImage,
      registerValidation,
      itemsControllers.registerItem
    )
    // .get(authentication, authorization, itemsControllers.getAllItems);
    .get(itemsControllers.getAllItems);

  app
    .route("/api/items/available")
    // .get(authentication, authorization, itemsControllers.getAvailableItems);
    .get(itemsControllers.getAvailableItems);

  app
    .route("/api/items/:name")
    // .get(authentication, authorization, itemsControllers.getItemByName)
    .get(itemsControllers.getItemByName)
    .put(
      authentication,
      authorization,
      itemImage,
      updateValidation,
      itemsControllers.updateItemByName
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      itemsControllers.deleteItemByName
    );

  app
    .route("/api/items/category/:categoryName")
    .get(authentication, authorization, itemsControllers.getAllItemsByCategory);
};

const authentication = require("../../middlewares/auth/authentication");
const authorization = require("../../middlewares/auth/authorization");
const {
  registerValidation,
  updateValidation,
  deleteValidation,
} = require("../../middlewares/validations/roles.validations");
const rolesControllers = require("../../controllers/roles.controllers");

module.exports = (app) => {
  app
    .route("/api/roles")
    .post(
      authentication,
      authorization,
      registerValidation,
      rolesControllers.registerRole
    )
    .get(authentication, authorization, rolesControllers.getAllRoles);

  app
    .route("/api/roles/:code")
    .get(authentication, authorization, rolesControllers.getRoleByCode)
    .put(
      authentication,
      authorization,
      updateValidation,
      rolesControllers.updateRoleByCode
    )
    .delete(
      authentication,
      authorization,
      deleteValidation,
      rolesControllers.deleteRoleByCode
    );
};

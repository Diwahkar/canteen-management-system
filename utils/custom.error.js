class ErrorClass extends Error {
  constructor(statusCode, success, name, message) {
    super(message);
    this.status = statusCode;
    this.success = success;
    this.name = name;
  }
}

module.exports = ErrorClass;

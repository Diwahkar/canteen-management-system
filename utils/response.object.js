module.exports = (statusCode, success, origin, message, data) => {
  const response = {
    status: statusCode || 500,
    success,
    origin,
    message,
  };
  if (data) response[Object.keys(data)[0]] = data[Object.keys(data)[0]];
  return response;
};

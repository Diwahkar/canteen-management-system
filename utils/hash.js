const bcrypt = require("bcrypt");

module.exports = {
  hash: async (password) => {
    return await bcrypt.hash(password, 10);
  },

  compare: async (password1, password2) => {
    return await bcrypt.compare(password1, password2);
  },
};

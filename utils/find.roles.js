const { userRoles } = require("../models");

module.exports = async (username) => {
  const allRoles = await userRoles.findAll({
    where: {
      username,
    },
  });
  const role = {
    isAdmin: false,
    isCashier: false,
  };
  if (allRoles?.some((role) => role.roleCode === "admin")) role.isAdmin = true;
  if (allRoles?.some((role) => role.roleCode === "cashier"))
    role.isCashier = true;
  return role;
};

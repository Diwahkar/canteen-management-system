const nodemailer = require("nodemailer");

module.exports = async (mailObject) => {
  const user = process.env.NODEMAILER_USER;
  const pass = process.env.NODEMAILER_PASS;

  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    auth: {
      user,
      pass,
    },
  });

  let mailOptions = {
    from: user,
    to: mailObject.receiverEmail,
    subject: `${mailObject.subject}`,
    html: `
    <h1>${mailObject.subject}</h1>
    <h2>Hello ${mailObject.receiverEmail}</h2>
    ${
      mailObject.emailVerifyCode
        ? "<p>Thank You for Subscribing.</p>" +
          `<p>Your Email Verify Code is ${mailObject.emailVerifyCode}</p>`
        : `<p>Your Reset Password Code is ${mailObject.resetForgottenPasswordCode}</p>`
    }    
    `,
  };
  const response = await transporter.sendMail(mailOptions);
  return response;
};

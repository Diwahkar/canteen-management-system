const Joi = require("joi");

module.exports = (input) => {
  const emailSchema = Joi.object({ email: Joi.string().email() });
  const { error } = emailSchema.validate({ email: input });
  if (error) return { username: input };
  return { email: input };
};

const jwt = require("jsonwebtoken");

module.exports = (username, secretKey) => {
  const payload = {
    sub: username,
  };
  return `${jwt.sign(payload, secretKey, { expiresIn: "1d" })}`;
};

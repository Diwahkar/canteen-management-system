"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class payments extends Model {
    static associate(models) {
      payments.belongsTo(models.orders);
      payments.belongsTo(models.transactions);
      payments.belongsTo(models.users, {
        foreignKey: "username",
        targetKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  payments.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      amount: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      amountReceived: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      amountReturned: {
        type: DataTypes.INTEGER,
      },
      remarks: {
        type: DataTypes.ENUM,
        values: ["surplus", "balance", "deficit"],
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "payments",
      deletedAt: "deletedAt",
      paranoid: true,
      hooks: {
        beforeSave: (payment, options) => {
          if (payment.amountReceived < payment.amount)
            payment.remarks = "deficit";
          else if (
            payment.amountReceived === payment.amount ||
            (payment.amountReceived > payment.amount &&
              payment.amountReturned > payment.amountReceived - payment.amount)
          )
            payment.remarks = "deficit";
          else if (
            payment.amountReceived === payment.amount ||
            (payment.amountReceived > payment.amount &&
              payment.amountReturned ===
                payment.amountReceived - payment.amount)
          )
            payment.remarks = "balance";
          else payment.remarks = "surplus";
        },
      },
    }
  );
  return payments;
};

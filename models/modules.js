"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class modules extends Model {
    static associate(models) {
      modules.hasMany(models.accesses, {
        foreignKey: "moduleCode",
        sourceKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      modules.hasMany(models.modulePrivileges, {
        foreignKey: "moduleCode",
        sourceKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  modules.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      code: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      desc: {
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "modules",
      deletedAt: "deletedAt",
      paranoid: true,
      hooks: {
        afterDestroy: async (module, options) => {
          const accesses = await module.getAccesses();
          accesses.forEach(async (access) => await access.destroy());

          const modulePrivileges = await module.getModulePrivileges();
          modulePrivileges.forEach(
            async (modulePrivilege) => await modulePrivilege.destroy()
          );
        },

        afterRestore: async (module, options) => {
          const accesses = await module.getAccesses({ paranoid: false });
          accesses.forEach(async (access) => await access.restore());

          const modulePrivileges = await module.getModulePrivileges({
            paranoid: false,
          });
          modulePrivileges.forEach(
            async (modulePrivilege) => await modulePrivilege.restore()
          );
        },
      },
    }
  );
  return modules;
};

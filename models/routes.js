"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class routes extends Model {
    static associate(models) {
      routes.belongsTo(models.privileges, {
        foreignKey: "privilegeCode",
        targetKey: "code",
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      });
    }
  }
  routes.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      method: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      url: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "routes",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return routes;
};

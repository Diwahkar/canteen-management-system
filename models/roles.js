"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class roles extends Model {
    static associate(models) {
      roles.hasMany(models.userRoles, {
        foreignKey: "roleCode",
        sourceKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      roles.hasMany(models.accesses, {
        foreignKey: "roleCode",
        sourceKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  roles.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      code: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      desc: {
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "roles",
      deletedAt: "deletedAt",
      paranoid: true,
      hooks: {
        afterDestroy: async (module, options) => {
          const userRoles = await module.getAccesses();
          userRoles.forEach(async (userRole) => await userRole.destroy());
        },

        afterRestore: async (module, options) => {
          const userRoles = await module.getAccesses({ paranoid: false });
          userRoles.forEach(async (userRole) => await userRole.restore());
        },
      },
    }
  );
  return roles;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class jwtSecrets extends Model {
    static associate(models) {
      jwtSecrets.belongsTo(models.users, {
        foreignKey: "username",
        targetKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  jwtSecrets.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },

      key: {
        allowNull: false,
        type: DataTypes.STRING,
      },

      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "jwtSecrets",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return jwtSecrets;
};

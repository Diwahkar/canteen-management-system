"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class accesses extends Model {
    static associate(models) {
      accesses.belongsTo(models.roles, {
        foreignKey: "roleCode",
        targetKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      accesses.belongsTo(models.modules, {
        foreignKey: "moduleCode",
        targetKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      accesses.belongsTo(models.privileges, {
        foreignKey: "privilegeCode",
        targetKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  accesses.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "accesses",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return accesses;
};

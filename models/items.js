"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class items extends Model {
    static associate(models) {
      items.belongsTo(models.categories, {
        foreignKey: "categoryName",
        targetKey: "name",
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      });
      items.hasMany(models.orderedItems, {
        foreignKey: "itemName",
        sourceKey: "name",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  items.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      price: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      image: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      soldQuantity: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      availableQuantity: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "items",
      deletedAt: "deletedAt",
      paranoid: true,
      hooks: {
        afterDestroy: function (item, options) {
          item.getOrderedItems().then((orderedItem) => orderedItem.destroy());
        },
        afterRestore: function (item, options) {
          item
            .getOrderedItems({ paranoid: false })
            .then((orderedItem) => orderedItem.restore());
        },
      },
    }
  );
  return items;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class orderedItems extends Model {
    static associate(models) {
      orderedItems.belongsTo(models.items, {
        foreignKey: "itemName",
        targetKey: "name",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      }),
        orderedItems.belongsTo(models.orders);
    }
  }
  orderedItems.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      quantity: {
        allowNull: false,
        defaultValue: 1,
        type: DataTypes.INTEGER,
      },
      total: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "orderedItems",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return orderedItems;
};

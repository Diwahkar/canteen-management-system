"use strict";
const { Model } = require("sequelize");
// const {orderedItems} = require('./')
module.exports = (sequelize, DataTypes) => {
  class orders extends Model {
    static associate(models) {
      orders.belongsTo(models.payments);
      orders.belongsTo(models.users, {
        foreignKey: "username",
        targetKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      orders.hasMany(models.orderedItems);
    }
  }
  orders.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      total: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        defaultValue: "just",
        type: DataTypes.ENUM,
        values: ["just", "processed", "ready"],
      },
      paid: {
        allowNull: false,
        defaultValue: false,
        type: DataTypes.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "orders",
      deletedAt: "deletedAt",
      paranoid: true,
      hooks: {
        afterDestroy: async (order, options) => {
          const orderedItems = await order.getOrderedItems();
          orderedItems.forEach(
            async (orderedItem) => await orderedItem.destroy()
          );
        },

        afterRestore: async (order, options) => {
          const orderedItems = await order.getOrderedItems({ paranoid: false });
          orderedItems.forEach(
            async (orderedItem) => await orderedItem.destroy()
          );
        },
      },
    }
  );
  return orders;
};

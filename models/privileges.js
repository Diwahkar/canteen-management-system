"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class privileges extends Model {
    static associate(models) {
      privileges.hasMany(models.accesses, {
        foreignKey: "privilegeCode",
        sourceKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      privileges.hasMany(models.modulePrivileges, {
        foreignKey: "privilegeCode",
        sourceKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      privileges.hasMany(models.routes, {
        foreignKey: "privilegeCode",
        sourceKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  privileges.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      code: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      desc: {
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "privileges",
      deletedAt: "deletedAt",
      paranoid: true,
      hooks: {
        afterDestroy: async (module, options) => {
          const accesses = await module.getAccesses();
          accesses.forEach(async (access) => await access.destroy());

          const modulePrivileges = await module.getModulePrivileges();
          modulePrivileges.forEach(
            async (modulePrivilege) => await modulePrivilege.destroy()
          );

          const routes = await module.getRoutes();
          routes.forEach(async (routes) => await routes.destroy());
        },

        afterRestore: async (module, options) => {
          const accesses = await module.getAccesses({ paranoid: false });
          accesses.forEach(async (access) => await access.restore());

          const modulePrivileges = await module.getModulePrivileges({
            paranoid: false,
          });
          modulePrivileges.forEach(
            async (modulePrivilege) => await modulePrivilege.destroy()
          );

          const routes = await module.getModulePrivileges({ paranoid: false });
          routes.forEach(async (route) => await route.restore());
        },
      },
    }
  );
  return privileges;
};

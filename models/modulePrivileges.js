"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class modulePrivileges extends Model {
    static associate(models) {
      modulePrivileges.belongsTo(models.modules, {
        foreignKey: "moduleCode",
        targetKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      modulePrivileges.belongsTo(models.privileges, {
        foreignKey: "privilegeCode",
        targetKey: "code",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  modulePrivileges.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "modulePrivileges",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return modulePrivileges;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class transactions extends Model {
    static associate(models) {
      transactions.belongsTo(models.payments, {
        onDelete: "CASCADE",
      });
      transactions.belongsTo(models.users, {
        foreignKey: "username",
        targetKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  transactions.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      deposit: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      withdrawn: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      onHandBalance: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "transactions",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return transactions;
};

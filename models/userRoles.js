"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class userRoles extends Model {
    static associate(models) {
      userRoles.belongsTo(models.users, {
        foreignKey: "username",
        targetKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      }),
        userRoles.belongsTo(models.roles, {
          foreignKey: "roleCode",
          targetKey: "code",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
        });
    }
  }
  userRoles.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "userRoles",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return userRoles;
};

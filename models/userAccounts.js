"use strict";
const { user } = require("pg/lib/defaults");
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class userAccounts extends Model {
    static associate(models) {
      userAccounts.belongsTo(models.users, {
        foreignKey: "username",
        targetKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  userAccounts.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      balance: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      creditLimit: {
        allowNull: false,
        defaultValue: 5000,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "userAccounts",
      deletedAt: "deletedAt",
      paranoid: true,
    }
  );
  return userAccounts;
};

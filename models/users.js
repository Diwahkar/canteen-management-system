"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      users.hasOne(models.jwtSecrets, {
        foreignKey: "username",
        sourceKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADe",
      });
      users.hasOne(models.userAccounts, {
        foreignKey: "username",
        sourceKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADe",
      });
      users.hasMany(models.userRoles, {
        foreignKey: "username",
        sourceKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      users.hasMany(models.orders, {
        foreignKey: "username",
        sourceKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      users.hasMany(models.payments, {
        foreignKey: "username",
        sourceKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
      users.hasMany(models.transactions, {
        foreignKey: "username",
        sourceKey: "username",
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      });
    }
  }
  users.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      username: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      email: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      pin: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      department: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      position: {
        allowNull: false,
        defaultValue: "junior",
        type: DataTypes.ENUM,
        values: ["senior", "mid", "junior"],
      },
      displayPic: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      emailVerified: {
        allowNull: false,
        defaultValue: false,
        type: DataTypes.BOOLEAN,
      },
      emailVerifyCode: {
        type: DataTypes.STRING,
      },
      forgotPassword: {
        defaultValue: false,
        type: DataTypes.BOOLEAN,
      },
      resetForgottenPasswordCode: {
        type: DataTypes.STRING,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "users",
      deletedAt: "deletedAt",
      paranoid: true,
      hooks: {
        afterDestroy: async (user, options) => {
          user.getJwtSecret().then((jwtSecret) => jwtSecret.destroy());

          const orders = await user.getOrders();
          orders.forEach(async (order) => await order.destroy());

          const payments = await user.getPayments();
          payments.forEach(async (payment) => await payment.destroy());

          const transactions = await user.getTransactions();
          transactions.forEach(
            async (transaction) => await transaction.destroy()
          );

          user.getUserAccount().then((userAccount) => userAccount.destroy());

          const userRoles = await user.getUserRoles();
          userRoles.forEach(async (userRole) => await userRole.destroy());
        },

        afterRestore: async (user, options) => {
          user
            .getJwtSecret({ paranoid: false })
            .then((jwtSecret) => jwtSecret.destroy());

          const orders = await user.getOrders({ paranoid: false });
          orders.forEach(async (order) => await order.destroy());

          const payments = await user.getPayments({ paranoid: false });
          payments.forEach(async (payment) => await payment.destroy());

          const transactions = await user.getTransactions({ paranoid: false });
          transactions.forEach(
            async (transaction) => await transaction.destroy()
          );

          user
            .getUserAccount({ paranoid: false })
            .then((userAccount) => userAccount.destroy());

          const userRoles = await user.getUserRoles({ paranoid: false });
          userRoles.forEach(async (userRole) => await userRole.destroy());
        },
      },
    }
  );
  return users;
};

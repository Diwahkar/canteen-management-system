"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("items", [
        {
          name: "black-tea",
          price: 30,
          image: "public/userImage/pic.jpeg",
          categoryName: "drinks",
          availableQuantity: 10,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "lemon-tea",
          price: 30,
          image: "public/userImage/pic.jpeg",
          categoryName: "drinks",
          availableQuantity: 10,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "sandwich",
          price: 60,
          image: "public/userImage/pic.jpeg",
          categoryName: "snacks",
          availableQuantity: 10,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "momo",
          price: 150,
          image: "public/userImage/pic.jpeg",
          categoryName: "fast food",
          availableQuantity: 10,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "aalu",
          price: 50,
          image: "public/userImage/pic.jpeg",
          categoryName: "meal",
          availableQuantity: 10,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "samosa",
          price: 30,
          image: "public/userImage/pic.jpeg",
          categoryName: "fast food",
          availableQuantity: 10,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("items", null, {});
    } catch (error) {
      throw new Error(error);
    }
  },
};

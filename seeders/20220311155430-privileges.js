"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("privileges", [
        {
          code: "create",
          desc: "post into model",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "read",
          desc: "read all from model",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "readBySlug",
          desc: "read specific Slug from model",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "update",
          desc: "update model instance",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "updateBySlug",
          desc: "update specific Slug in model",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "delete",
          desc: "delete model instant",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "deleteBySlug",
          desc: "delete specific Slug from model",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("privileges", null, {});
    } catch (error) {
      throw new Error(error);
    }
  },
};

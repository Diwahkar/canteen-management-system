"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("roles", [
        {
          code: "admin",
          desc: "super user for this project",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "cashier",
          desc: "orders-payments-transactions and other related privileges",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "customer",
          desc: "canteen customer",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("roles", null, {});
    } catch (error) {
      throw new Error(error);
    }
  },
};

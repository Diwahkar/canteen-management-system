"use strict";

const { hash } = require("../utils/hash");
const crypto = require("crypto");
module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("users", [
        {
          name: "Super User",
          username: "super",
          email: "49w1k1r@gmail.com",
          password: await hash("Abcd@123"),
          pin: await hash("0000"),
          department: "administration",
          position: "senior",
          displayPic:
            "http://res.cloudinary.com/dwdbn04c9/image/upload/v1648881465/hu0n8xmiqdx9zldapw9y.jpg",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
      await queryInterface.bulkInsert("userRoles", [
        {
          username: "super",
          roleCode: "admin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "super",
          roleCode: "cashier",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "super",
          roleCode: "customer",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);

      await queryInterface.bulkInsert("userAccounts", [
        {
          username: "super",
          balance: 0,
          creditLimit: 15000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);

      const jwtSecretKey = crypto.randomBytes(8).toString("hex");
      await queryInterface.bulkInsert("jwtSecrets", [
        {
          username: "super",
          key: jwtSecretKey,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);

      await queryInterface.sequelize.query(
        `UPDATE "users" SET "emailVerified"='true', "emailVerifyCode"='null' WHERE "username"='super'`,
        { type: queryInterface.sequelize.QueryTypes.UPDATE }
      );
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("users", { username: "super" }, {});
      await queryInterface.bulkDelete("userRoles", { username: "super" }, {});
      await queryInterface.bulkDelete(
        "userAccounts",
        { username: "super" },
        {}
      );
    } catch (error) {
      throw new Error(error);
    }
  },
};

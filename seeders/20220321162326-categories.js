"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("categories", [
        {
          name: "drinks",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "snacks",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "fast food",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "meal",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("categories", null, {});
    } catch (error) {
      throw new Error(error);
    }
  },
};

"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("modules", [
        {
          code: "accesses",
          desc: "authorization table based on roles-modules-privileges tables",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "categories",
          desc: "item categories",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "items",
          desc: "cateen menu items",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "modulePrivileges",
          desc: "mapping table for module-privileges tables",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "modules",
          desc: "list of all modules in this project",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "orderedItems",
          desc: "items ordered by customers",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "orders",
          desc: "orders of custoemers",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "payments",
          desc: "payments of customers",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "privileges",
          desc: "lists all privileges available irrespective of modules",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "roles",
          desc: "roles for authorization",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "routes",
          desc: "protected routes",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "transactions",
          desc: "user transactions",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "userAccounts",
          desc: "customer account for debit-credit records",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "userRoles",
          desc: "mapping table for roles and users",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          code: "users",
          desc: "details about users",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("modules", null, {});
    } catch (error) {
      throw new Error(error);
    }
  },
};

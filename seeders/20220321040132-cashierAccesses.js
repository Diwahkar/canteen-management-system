"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("accesses", [
        {
          roleCode: "cashier",
          moduleCode: "categories",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "categories",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "categories",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "items",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "items",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "items",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "orderedItems",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "orderedItems",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "payments",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "transactions",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "userAccounts",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "cashier",
          moduleCode: "users",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("accesses", { roleCode: "cashier" }, {});
    } catch (error) {
      throw new Error(error);
    }
  },
};

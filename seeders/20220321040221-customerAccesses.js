"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("accesses", [
        {
          roleCode: "customer",
          moduleCode: "categories",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "categories",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "items",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "items",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "orders",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "orders",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "orders",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "orders",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "orders",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "payments",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "payments",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "transactions",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "transactions",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "userAccounts",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "users",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "users",
          privilegeCode: "update",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "customer",
          moduleCode: "users",
          privilegeCode: "delete",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("accesses", { roleCode: "customer" }, {});
    } catch (error) {
      throw new Error(error);
    }
  },
};

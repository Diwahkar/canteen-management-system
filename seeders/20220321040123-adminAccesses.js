"use strict";
const { modules, privileges } = require("../models");

module.exports = {
  async up(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkInsert("accesses", [
        {
          roleCode: "admin",
          moduleCode: "accesses",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "accesses",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "accesses",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "accesses",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "accesses",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },

        {
          roleCode: "admin",
          moduleCode: "modulePrivileges",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modulePrivileges",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modulePrivileges",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modulePrivileges",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modulePrivileges",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modules",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modules",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modules",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modules",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "modules",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },

        {
          roleCode: "admin",
          moduleCode: "privileges",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "privileges",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "privileges",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "privileges",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "privileges",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "roles",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "roles",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "roles",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "roles",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "roles",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "routes",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "routes",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "routes",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "routes",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "routes",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "userRoles",
          privilegeCode: "create",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "userRoles",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "userRoles",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "userRoles",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "userRoles",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "userRoles",
          privilegeCode: "readBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "users",
          privilegeCode: "read",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "users",
          privilegeCode: "updateBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          roleCode: "admin",
          moduleCode: "users",
          privilegeCode: "deleteBySlug",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]);
    } catch (error) {
      throw new Error(error);
    }
  },

  async down(queryInterface, Sequelize) {
    try {
      await queryInterface.bulkDelete("accesses", { roleCode: "admin" }, {});
    } catch (error) {
      console.log(code);
      throw new Error(error);
    }
  },
};

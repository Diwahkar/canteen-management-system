const userAccountsServices = require("../services/userAccounts.services");

class UserAccountsControllers {
  async getAllUserAccounts(req, res, next) {
    try {
      const response = await userAccountsServices.getAllUserAccounts(req.user);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllUserAccounts";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getUserAccountByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await userAccountsServices.getUserAccountByUsername(
        username
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getUserAccountByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new UserAccountsControllers();

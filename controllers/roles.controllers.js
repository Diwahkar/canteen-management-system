const rolesServices = require("../services/roles.services");

class RolesControllers {
  async registerRole(req, res, next) {
    try {
      const response = await rolesServices.registerRole(req.user, req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "Register";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllRoles(req, res, next) {
    try {
      const response = await rolesServices.getAllRoles();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllRoles";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getRoleByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await rolesServices.getRoleByCode(code);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getRoleByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateRoleByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await rolesServices.updateRoleByCode(
        req.user,
        code,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateRoleByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteRoleByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await rolesServices.deleteRoleByCode(
        req.user,
        code,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteRoleByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new RolesControllers();

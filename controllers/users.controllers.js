const usersServices = require("../services/users.services");

class usersControllers {
  async registerUser(req, res, next) {
    try {
      const response = await usersServices.registerUser(req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "register";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllUsers(req, res, next) {
    try {
      const response = await usersServices.getAllUsers(req.user);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllUsers";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateMyProfile(req, res, next) {
    try {
      const response = await usersServices.updateMyProfile(req.user, req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "userController updateMyProfile";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteMyProfile(req, res, next) {
    try {
      const response = await usersServices.deleteMyProfile(req.user, req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "userController deleteMyProfile";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async emailVerify(req, res, next) {
    try {
      const response = await usersServices.emailVerify(req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "emailVerify";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async loginUser(req, res, next) {
    try {
      const response = await usersServices.loginUser(req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "loginUser";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async forgotPassword(req, res, next) {
    try {
      const response = await usersServices.forgotPassword(req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "forgotPassword";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async resetForgottenPassword(req, res, next) {
    try {
      const response = await usersServices.resetForgottenPassword(req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "resetPassword";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getUserByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await usersServices.getUserByUsername(username);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "userController getUserByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateUserByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await usersServices.updateUserByUsername(
        req.user,
        username,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "userController updateUserByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteUserByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await usersServices.deleteUserByUsername(
        req.user,
        username,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "userController deleteUserByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new usersControllers();

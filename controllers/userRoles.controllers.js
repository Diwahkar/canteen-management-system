const userRolesServices = require("../services/userRoles.services");

class UserRolesControllers {
  async registerUserRole(req, res, next) {
    try {
      const response = await userRolesServices.registerUserRole(
        req.user,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerUserRole";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllUserRoles(req, res, next) {
    try {
      const response = await userRolesServices.getAllUserRoles();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllUserRoles";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getUserRoleById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await userRolesServices.getUserRoleById(id);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getUserRoleById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateUserRoleById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await userRolesServices.updateUserRoleById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateUserRoleById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteUserRoleById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await userRolesServices.deleteUserRoleById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteUserRoleById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllUserRolesByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await userRolesServices.getAllUserRolesByUsername(
        username
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllUserRolesByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new UserRolesControllers();

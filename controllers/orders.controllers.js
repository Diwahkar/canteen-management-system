const ordersServices = require("../services/orders.services");

class OrdersControllers {
  async registerOrder(req, res, next) {
    try {
      const response = await ordersServices.registerOrder(req.user, req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerOrder";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllOrders(req, res, next) {
    try {
      const response = await ordersServices.getAllOrders(req.user);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllOrders";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getOrderById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await ordersServices.getOrderById(req.user, id);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getOrderById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateOrderById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await ordersServices.updateOrderById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateOrderById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteOrderById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await ordersServices.deleteOrderById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteOrderById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllOrdersByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await ordersServices.getAllOrdersByUsername(
        req.user,
        username
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllOrdersByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateOrderStatusById(req, res, next) {
    try {
      const { id, status } = req.params;
      const response = await ordersServices.updateOrderStatusById(
        req.user,
        id,
        status
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateOrderStatusById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new OrdersControllers();

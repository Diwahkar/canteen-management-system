const socketIoService = require("../services/socket.io.services");

const initializeSocketIo = async (io) => {
  let user;
  io.use(async (socket, next) => {
    try {
      user = await socketIoService.loginUser(socket.handshake.auth);
      next();
    } catch (error) {
      return next(error);
    }
  });

  io.on("connection", async (socket) => {
    socket.join(user.username);

    socket.on("postOrder", async (data) => {
      try {
        return await socketIoService.postOrder(user, data, socket);
      } catch (error) {
        return socketIoService.postOrderError(error, socket);
      }
    });

    socket.on("orderStatus", async (orderId) => {
      try {
        return await socketIoService.orderStatus(user, orderId, socket);
      } catch (error) {
        return socketIoService.orderStatusError(error, socket);
      }
    });

    socketIoService.func();

    socketIoService.socketEmitterInstance.on("orderReady", (data) => {
      try {
        return socketIoService.emitter(data, socket);
      } catch (error) {
        return socketIoService.emitterError(error, socket);
      }
    });
  });
};

module.exports = {
  initializeSocketIo,
};

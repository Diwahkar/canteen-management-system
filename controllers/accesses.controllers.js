const accessesServices = require("../services/accesses.services");

class AccessesControllers {
  async registerAccess(req, res, next) {
    try {
      const response = await accessesServices.registerAccess(
        req.user,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerAccess";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllAccesses(req, res, next) {
    try {
      const response = await accessesServices.getAllAccesses();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllAccesss";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAccessById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await accessesServices.getAccessById(id);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAccessById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateAccessById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await accessesServices.updateAccessById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateAccessById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteAccessById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await accessesServices.deleteAccessById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteAccessById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllAccessesByRole(req, res, next) {
    try {
      const roleCode = req.params.roleCode;
      const response = await accessesServices.getAllAccessesByRole(roleCode);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllAccessesByRole";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllAccessesByModule(req, res, next) {
    try {
      const moduleCode = req.params.moduleCode;
      const response = await accessesServices.getAllAccessesByModule(
        moduleCode
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllAccessesByModule";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllAccessesByPrivilege(req, res, next) {
    try {
      const privilegeCode = req.params.privilegeCode;
      const response = await accessesServices.getAllAccessesByPrivilege(
        privilegeCode
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllAccessesByPrivilege";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new AccessesControllers();

const transactionService = require("../services/transactions.services");

class TransactionsControllers {
  async registerTransaction(req, res, next) {
    try {
      const response = await transactionService.registerTransaction(
        req.user,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerTransaction";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllTransactions(req, res, next) {
    try {
      const response = await transactionService.getAllTransactions(req.user);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getMyTransactions";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getTransactionById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await transactionService.getTransactionById(
        req.user,
        id
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getTransactionById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllTransactionsByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await transactionService.getAllTransactionsByUsername(
        req.user,
        username
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllTransactionsByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new TransactionsControllers();

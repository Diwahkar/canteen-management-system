const itemsServices = require("../services/items.services");

class ItemsControllers {
  async registerItem(req, res, next) {
    try {
      const response = await itemsServices.registerItem(req.user, req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerItem";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllItems(req, res, next) {
    try {
      const response = await itemsServices.getAllItems();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || error.name || "getAllItems";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAvailableItems(req, res, next) {
    try {
      const response = await itemsServices.getAvailableItems();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || error.name || "getAvailableItems";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getItemByName(req, res, next) {
    try {
      const name = req.params.name;
      const response = await itemsServices.getItemByName(name);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getItemByName";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateItemByName(req, res, next) {
    try {
      const name = req.params.name;
      const response = await itemsServices.updateItemByName(
        req.user,
        name,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateItemByName";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteItemByName(req, res, next) {
    try {
      const name = req.params.name;
      const response = await itemsServices.deleteItemByName(
        req.user,
        name,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteItemByName";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllItemsByCategory(req, res, next) {
    try {
      const categoryName = req.params.categoryName;
      const response = await itemsServices.getAllItemsByCategory(categoryName);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllItemsByCategory";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new ItemsControllers();

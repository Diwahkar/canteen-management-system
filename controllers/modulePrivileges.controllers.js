const modulePrivilegeServices = require("../services/modulePrivileges.services");

class ModulePrivilegeControllers {
  async registerModulePrivilege(req, res, next) {
    try {
      const response = await modulePrivilegeServices.registerModulePrivilege(
        req.user,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerModulePrivilege";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllModulePrivileges(req, res, next) {
    try {
      const response = await modulePrivilegeServices.getAllModulePrivileges();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllModulePrivileges";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getModulePrivilegeById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await modulePrivilegeServices.getModulePrivilegeById(id);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getModulePrivilegeById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateModulePrivilegeById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await modulePrivilegeServices.updateModulePrivilegeById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateModulePrivilegeById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteModulePrivilegeById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await modulePrivilegeServices.deleteModulePrivilegeById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteModulePrivilegeById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllModulePrivilegesByModule(req, res, next) {
    try {
      const moduleCode = req.params.moduleCode;
      const response =
        await modulePrivilegeServices.getAllModulePrivilegesByModule(
          moduleCode
        );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllModulePrivilegesByModule";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllModulePrivilegesByPrivilege(req, res, next) {
    try {
      const privilegeCode = req.params.privilegeCode;
      const response =
        await modulePrivilegeServices.getAllModulePrivilegesByPrivilege(
          privilegeCode
        );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllModulePrivilegesByPrivilege";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new ModulePrivilegeControllers();

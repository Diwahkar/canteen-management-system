const orderedItemsServices = require("../services/orderedItems.services");

class OrderedItemsControllers {
  async getAllOrderedItems(req, res, next) {
    try {
      const response = await orderedItemsServices.getAllOrderedItems();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllOrderedItems";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getOrderedItemById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await orderedItemsServices.getOrderedItemById(id);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getOrderedItemById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllOrderedItemsByOrderId(req, res, next) {
    try {
      const orderId = req.params.orderId;
      const response = await orderedItemsServices.getAllOrderedItemsByOrderId(
        orderId
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllOrderedItemsByOrderId";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new OrderedItemsControllers();

const privilegesServices = require("../services/privileges.services");

class PrivilegesControllers {
  async registerPrivilege(req, res, next) {
    try {
      const response = await privilegesServices.registerPrivilege(
        req.user,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerPrivilege";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllPrivileges(req, res, next) {
    try {
      const response = await privilegesServices.getAllPrivileges();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllPrivileges";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getPrivilegeByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await privilegesServices.getPrivilegeByCode(code);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getPrivilegeByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updatePrivilegeByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await privilegesServices.updatePrivilegeByCode(
        req.user,
        code,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updatePrivilegeByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deletePrivilegeByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await privilegesServices.deletePrivilegeByCode(
        req.user,
        code,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deletePrivilegeByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new PrivilegesControllers();

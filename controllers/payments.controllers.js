const paymentsServices = require("../services/payments.services");

class PaymentsControllers {
  async registerPayment(req, res, next) {
    try {
      const response = await paymentsServices.registerPayment(
        req.user,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerPayment";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllPayments(req, res, next) {
    try {
      const response = await paymentsServices.getAllPayments(req.user);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllMyPayments";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getPaymentById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await paymentsServices.getPaymentById(req.user, id);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getPaymentById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllPaymentsByUsername(req, res, next) {
    try {
      const username = req.params.username;
      const response = await paymentsServices.getAllPaymentsByUsername(
        req.user,
        username
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllPaymentsByUsername";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new PaymentsControllers();

const routesServices = require("../services/routes.services");

class RoutesControllers {
  async registerRoute(req, res, next) {
    try {
      const response = await routesServices.registerRoute(req.user, req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerRoute";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllRoutes(req, res, next) {
    try {
      const response = await routesServices.getAllRoutes();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllRoutes";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getRouteById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await routesServices.getRouteById(id);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getRouteById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateRouteById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await routesServices.updateRouteById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateRouteById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteRouteById(req, res, next) {
    try {
      const id = req.params.id;
      const response = await routesServices.deleteRouteById(
        req.user,
        id,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteRouteById";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllRoutesByPrivilege(req, res, next) {
    try {
      const privilegeCode = req.params.privilegeCode;
      const response = await routesServices.getAllRoutesByPrivilege(
        privilegeCode
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllRoutesByPrivilege";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new RoutesControllers();

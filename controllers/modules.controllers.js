const modulesServices = require("../services/modules.services");

class ModulesControllers {
  async registerModule(req, res, next) {
    try {
      const response = await modulesServices.registerModule(req.user, req.body);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "Register";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllModules(req, res, next) {
    try {
      const response = await modulesServices.getAllModules();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllModules";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getModuleByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await modulesServices.getModuleByCode(code);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getModuleByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateModuleByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await modulesServices.updateModuleByCode(
        req.user,
        code,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateModuleByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteModuleByCode(req, res, next) {
    try {
      const code = req.params.code;
      const response = await modulesServices.deleteModuleByCode(
        req.user,
        code,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteModuleByCode";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new ModulesControllers();

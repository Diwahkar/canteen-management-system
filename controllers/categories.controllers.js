const categoriesServices = require("../services/categories.services");

class CategoriesControllers {
  async registerCategory(req, res, next) {
    try {
      const response = await categoriesServices.registerCategory(
        req.user,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "registerCategory";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getAllCategories(req, res, next) {
    try {
      const response = await categoriesServices.getAllCategories();
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getAllCategories";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async getCategoryByName(req, res, next) {
    try {
      const name = req.params.name;
      const response = await categoriesServices.getCategoryByName(name);
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "getCategoryByName";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async updateCategoryByName(req, res, next) {
    try {
      const name = req.params.name;
      const response = await categoriesServices.updateCategoryByName(
        req.user,
        name,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "updateCategoryByName";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }

  async deleteCategoryByName(req, res, next) {
    try {
      const name = req.params.name;
      const response = await categoriesServices.deleteCategoryByName(
        req.user,
        name,
        req.body
      );
      res.status(response.status).json(response);
    } catch (error) {
      error.origin = error.name || "deleteCategoryByName";
      error.errors?.map((i) => (error.message = i.message));
      next(error);
    }
  }
}

module.exports = new CategoriesControllers();

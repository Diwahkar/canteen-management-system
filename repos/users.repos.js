const { users } = require("../models");

class UserRepos {
  async create(data, transaction) {
    return await users.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await users.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await users.findOne(condition, { transaction });
  }

  async update(data, user, transaction) {
    await user.update(data, { transaction });
  }

  async delete(user, transaction) {
    await user.destroy({ transaction });
  }
}

module.exports = new UserRepos();

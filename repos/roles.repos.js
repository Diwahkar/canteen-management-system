const { roles } = require("../models");

class RoleRepos {
  async create(data, transaction) {
    return await roles.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await roles.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await roles.findOne(condition, { transaction });
  }

  async update(data, role, transaction) {
    await role.update(data, { transaction });
  }

  async delete(role, transaction) {
    await role.destroy({ transaction });
  }
}

module.exports = new RoleRepos();

const { privileges } = require("../models");

class PrivilegeRepos {
  async create(data, transaction) {
    return await privileges.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await privileges.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await privileges.findOne(condition, { transaction });
  }

  async update(data, privilege, transaction) {
    await privilege.update(data, { transaction });
  }

  async delete(privilege, transaction) {
    await privilege.destroy({ transaction });
  }
}

module.exports = new PrivilegeRepos();

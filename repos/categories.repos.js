const { categories } = require("../models");

class CategoriesRepos {
  async create(data, transaction) {
    return await categories.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await categories.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await categories.findOne(condition, { transaction });
  }

  async update(data, category, transaction) {
    await category.update(data, { transaction });
  }

  async delete(category, transaction) {
    await category.destroy({ transaction });
  }
}

module.exports = new CategoriesRepos();

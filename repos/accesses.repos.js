const { accesses } = require("../models");

class AccessRepos {
  async create(data, transaction) {
    return await accesses.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await accesses.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await accesses.findOne(condition, { transaction });
  }

  async update(data, access, transaction) {
    await access.update(data, { transaction });
  }

  async delete(access, transaction) {
    await access.destroy({ transaction });
  }
}

module.exports = new AccessRepos();

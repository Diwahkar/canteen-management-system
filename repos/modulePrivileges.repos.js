const { modulePrivileges } = require("../models");

class ModulePrivilegeRepos {
  async create(data, transaction) {
    return await modulePrivileges.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await modulePrivileges.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await modulePrivileges.findOne(condition, { transaction });
  }

  async update(data, modulePrivilege, transaction) {
    await modulePrivilege.update(data, { transaction });
  }

  async delete(modulePrivilege, transaction) {
    await modulePrivilege.destroy({ transaction });
  }
}

module.exports = new ModulePrivilegeRepos();

const { routes } = require("../models");

class RouteRepos {
  async create(data, transaction) {
    return await routes.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await routes.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await routes.findOne(condition, { transaction });
  }

  async update(data, route, transaction) {
    await route.update(data, { transaction });
  }

  async delete(route, transaction) {
    await route.destroy({ transaction });
  }
}

module.exports = new RouteRepos();

const { userAccounts } = require("../models");

class UserAccountRepos {
  async create(data, transaction) {
    return await userAccounts.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await userAccounts.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await userAccounts.findOne(condition, { transaction });
  }

  async update(data, userAccount, transaction) {
    await userAccount.update(data, { transaction });
  }

  async delete(userAccount, transaction) {
    await userAccount.destroy({ transaction });
  }
}

module.exports = new UserAccountRepos();

const { orderedItems } = require("../models");

class OrderedItemRepos {
  async bulkCreate(data, transaction) {
    return await orderedItems.bulkCreate(data, { transaction });
  }

  async create(data, transaction) {
    return await orderedItems.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await orderedItems.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await orderedItems.findOne(condition, { transaction });
  }

  async update(data, orderedItem, transaction) {
    await orderedItem.update(data, { transaction });
  }

  async delete(orderedItem, transaction) {
    await orderedItem.destroy({ transaction });
  }
}

module.exports = new OrderedItemRepos();

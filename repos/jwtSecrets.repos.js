const { jwtSecrets } = require("../models");

class JWTSecretRepos {
  async create(data, transaction) {
    return await jwtSecrets.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await jwtSecrets.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await jwtSecrets.findOne(condition, { transaction });
  }

  async update(data, jwtSecret, transaction) {
    await jwtSecret.update(data, { transaction });
  }

  async delete(jwtSecret, transaction) {
    await jwtSecret.destroy({ transaction });
  }
}

module.exports = new JWTSecretRepos();

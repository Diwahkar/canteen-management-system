const { orders } = require("../models");

class OrderRepos {
  async create(data, transaction) {
    return await orders.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await orders.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await orders.findOne(condition, { transaction });
  }

  async update(data, order, transaction) {
    await order.update(data, { transaction });
  }

  async delete(order, transaction) {
    await order.destroy({ transaction });
  }
}

module.exports = new OrderRepos();

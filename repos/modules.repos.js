const { modules } = require("../models");

class ModuleRepos {
  async create(data, transaction) {
    return await modules.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await modules.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await modules.findOne(condition, { transaction });
  }

  async update(data, module, transaction) {
    await module.update(data, { transaction });
  }

  async delete(module, transaction) {
    await module.destroy({ transaction });
  }
}

module.exports = new ModuleRepos();

const { transactions } = require("../models");

class TransactionRepos {
  async create(data, transaction) {
    return await transactions.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await transactions.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await transactions.findOne(condition, { transaction });
  }

  async update(data, transactionsInstance, transaction) {
    await transactionsInstance.update(data, { transaction });
  }

  async delete(transactionsInstance, transaction) {
    await transactionsInstance.destroy({ transaction });
  }
}

module.exports = new TransactionRepos();

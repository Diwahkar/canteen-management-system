const { payments } = require("../models");

class PaymentRepos {
  async create(data, transaction) {
    return await payments.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await payments.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await payments.findOne(condition, { transaction });
  }

  async update(data, payment, transaction) {
    await payment.update(data, { transaction });
  }

  async delete(payment, transaction) {
    await payment.destroy({ transaction });
  }
}

module.exports = new PaymentRepos();

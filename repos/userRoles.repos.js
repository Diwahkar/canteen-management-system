const { userRoles } = require("../models");

class UserRoleRepos {
  async create(data, transaction) {
    return await userRoles.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await userRoles.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await userRoles.findOne(condition, { transaction });
  }

  async update(data, userRole, transaction) {
    await userRole.update(data, { transaction });
  }

  async delete(userRole, transaction) {
    await userRole.destroy({ transaction });
  }
}

module.exports = new UserRoleRepos();

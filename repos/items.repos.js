const { items } = require("../models");

class ItemRepos {
  async create(data, transaction) {
    return await items.create(data, { transaction });
  }

  async findAll(condition, transaction) {
    return await items.findAll(condition, { transaction });
  }

  async findOne(condition, transaction) {
    return await items.findOne(condition, { transaction });
  }

  async update(data, item, transaction) {
    return await item.update(data, { transaction });
  }

  async delete(item, transaction) {
    return await item.destroy({ transaction });
  }
}

module.exports = new ItemRepos();

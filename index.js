require("dotenv").config();
const { sequelize } = require("./models");

const nodemailer = require("./utils/nodemailer");

const cors = require("cors");
const express = require("express");
const app = express();
app.use(express.json({ limit: "5mb" }));

const http = require("http");
const httpServer = http.createServer(app);
const { Server } = require("socket.io");

sequelize
  .authenticate()
  .then(() => console.log("Connected to Database"))
  .catch((error) => {
    console.log(error.message);
    process.exit(1);
  });

app.use(cors());
require("./routes")(app);
require("./");
const io = new Server(httpServer, {
  cors: {
    origin: "*",
  },
});
require("./controllers/socket.io.controllers").initializeSocketIo(io);

app.use(require("./middlewares/unknown.route"));
app.use(require("./middlewares/error.handler"));

// setInterval(async () => {
//   await require("./services/users.services").deleteUsersWithUnverfiedEmails();
// }, 1000 * 60 * 60 * 24);

const PORT = process.env.PORT || 3000;
httpServer.listen(PORT, () =>
  console.log(`Server is listening on PORT ${PORT}`)
);

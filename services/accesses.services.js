const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const accessRepos = require("../repos/accesses.repos");
const roleRepos = require("../repos/roles.repos");
const moduleRepos = require("../repos/modules.repos");
const privilegeRepos = require("../repos/privileges.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class AccessesServices {
  async registerAccess(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "registerAccess",
          `Incorrect Password`
        );
      }
      const newAccess = await accessRepos.create(reqBody, t);
      return responseObject(200, true, "registerAccess", `Access Registered`, {
        newAccess: _.omit(newAccess.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllAccesses() {
    const response = sequelize.transaction(async (t) => {
      const allAccesses = await accessRepos.findAll(
        {
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(200, true, "getAllAccess", "All Accesses", {
        allAccesses,
      });
    });
    return response;
  }

  async getAccessById(id) {
    const response = sequelize.transaction(async (t) => {
      const access = await accessRepos.findOne(
        {
          where: { id },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!access) {
        throw new ErrorClass(
          404,
          false,
          "getAccessById",
          `Access "${id}" Not Found`
        );
      }
      return responseObject(
        200,
        true,
        "getAccessById",
        `Access "${id}"`,
        access
      );
    });
    return response;
  }

  async updateAccessById(reqUser, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateAccessById",
          `Incorrect Password`
        );
      }
      const access = await accessRepos.findOne(
        {
          where: { id },
        },
        t
      );
      if (!access) {
        throw new ErrorClass(
          404,
          false,
          "updateAccessById",
          `Access "${id}" Not Found`
        );
      }
      await accessRepos.update(
        {
          roleCode: reqBody.roleCode || access.roleCode,
          moduleCode: reqBody.moduleCode || access.moduleCode,
          privilegeCode: reqBody.privilegeCode || access.privilegeCode,
        },
        access,
        t
      );
      return responseObject(
        200,
        true,
        "updateAccessById",
        `Access "${id}" Updated`,
        {
          updatedAccess: _.omit(access.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deleteAccessById(reqUser, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deleteAccessById",
          `Incorrect Password`
        );
      }
      const access = await accessRepos.findOne(
        {
          where: { id },
        },
        t
      );
      if (!access) {
        throw new ErrorClass(
          404,
          false,
          "deleteAccessById",
          `Access "${id}" Not Found`
        );
      }
      await accessRepos.delete(access, t);
      return responseObject(
        200,
        true,
        "deleteAccessById",
        `Access "${id}" Deleted.`,
        null
      );
    });
    return response;
  }

  async getAllAccessesByRole(roleCode) {
    const response = sequelize.transaction(async (t) => {
      const role = await roleRepos.findOne(
        {
          where: { code: roleCode },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!role) {
        throw new ErrorClass(
          404,
          false,
          "getAllAccessesByRole",
          `Role "${roleCode}" Not Found`
        );
      }
      const allAccesses = await accessRepos.findAll(
        {
          where: { roleCode },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllAccessesByRole",
        `Accesses with RoleCode "${roleCode}"`,
        { allAccesses }
      );
    });
    return response;
  }

  async getAllAccessesByModule(moduleCode) {
    const response = sequelize.transaction(async (t) => {
      const module = await moduleRepos.findOne(
        {
          where: { code: moduleCode },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!module) {
        throw new ErrorClass(
          404,
          false,
          "getAllAccessesByModule",
          `Module "${moduleCode}" Not Found`
        );
      }
      const allAccesses = await accessRepos.findAll(
        {
          where: { moduleCode },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllAccessesByModule",
        `Access with ModuleCode "${moduleCode}"`,
        { allAccesses }
      );
    });
    return response;
  }

  async getAllAccessesByPrivilege(privilegeCode) {
    const response = sequelize.transaction(async (t) => {
      const privilege = await privilegeRepos.findOne(
        {
          where: {
            code: privilegeCode,
          },
          attributes: {
            exclude: ["createdAt", "updatedAt", "deletedAt"],
          },
        },
        t
      );
      if (!privilege) {
        throw new ErrorClass(
          404,
          false,
          "getAllAccessesByModule",
          `Privilege "${privilegeCode}" Not Found`
        );
      }
      const allAccesses = await accessRepos.findAll(
        {
          where: {
            privilegeCode,
          },
          attributes: {
            exclude: ["createdAt", "updatedAt", "deletedAt"],
          },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllAccessesByPrivilege",
        `Accesses with PrivilegeCode "${privilegeCode}"`,
        { allAccesses }
      );
    });
    return response;
  }
}

module.exports = new AccessesServices();

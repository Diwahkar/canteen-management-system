const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const rolesRepos = require("../repos/roles.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class RolesServices {
  async registerRole(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(403, false, "registerRole", `Incorrect Password`);
      }
      const newRole = await rolesRepos.create(reqBody, t);
      return responseObject(200, true, "registerRole", `Role Registered`, {
        newRole: _.omit(newRole.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllRoles() {
    const response = sequelize.transaction(async (t) => {
      const allRoles = await rolesRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(
        200,
        true,
        "RoleService(getAllRoles)",
        "All Roles",
        {
          allRoles,
        }
      );
    });
    return response;
  }

  async getRoleByCode(code) {
    const response = sequelize.transaction(async (t) => {
      const role = await rolesRepos.findOne(
        {
          where: {
            code,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!role) {
        throw new ErrorClass(
          404,
          false,
          "getRoleByCode",
          `Role "${code}" Not found`
        );
      }
      return responseObject(200, true, "getRoleByCode", `Role "${code}"`, {
        role,
      });
    });
    return response;
  }

  async updateRoleByCode(admin, code, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          404,
          false,
          "updateRoleByCode",
          `Incorrect Password`
        );
      }
      const role = await rolesRepos.findOne(
        {
          where: {
            code,
          },
        },
        t
      );
      if (!role) {
        throw new ErrorClass(
          404,
          false,
          "updateRoleByCode",
          `Role "${code}" Not Found`
        );
      }
      await rolesRepos.update(
        {
          code: reqBody.code,
          desc: reqBody.desc,
        },
        role,
        t
      );

      return responseObject(
        200,
        true,
        "updateRoleByCode",
        `Role "${code}" Updated`,
        {
          updatedRole: _.omit(role.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deleteRoleByCode(admin, code, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const role = await rolesRepos.findOne({ where: { code } }, t);
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          404,
          false,
          "deleteRoleByCode",
          `Incorrect Password`
        );
      }
      if (!role) {
        throw new ErrorClass(
          404,
          false,
          "deleteRoleByCode",
          `Role "${code}" Not Found`
        );
      }
      await rolesRepos.delete(role, t);
      return responseObject(
        200,
        true,
        "deleteRoleByCode",
        `Role "${code}" Deleted.`,
        null
      );
    });
    return response;
  }
}

module.exports = new RolesServices();

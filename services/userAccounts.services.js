const { sequelize } = require("../models");
const findRoles = require("../utils/find.roles");
const userAccountsRepos = require("../repos/userAccounts.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class UserAccountsServices {
  async getAllUserAccounts(reqUser) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier) {
        const allUserAccounts = await userAccountsRepos.findAll(
          { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
          t
        );
        return responseObject(
          200,
          true,
          "getAllUserAccounts",
          "All UserAccounts",
          { allUserAccounts }
        );
      }
      const myAccount = await userAccountsRepos.findAll(
        {
          where: {
            username: reqUser.username,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllUserAccounts",
        "All UserAccounts",
        { myAccount }
      );
    });
    return response;
  }

  async getUserAccountByUsername(username) {
    const response = sequelize.transaction(async (t) => {
      const userAccount = await userAccountsRepos.findOne(
        {
          where: {
            username,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!userAccount) {
        throw new ErrorClass(
          403,
          false,
          "getUserAccountByUsername",
          `UserAccount ${username} Not found`
        );
      }
      return responseObject(
        200,
        true,
        "getUserAccountByUsername",
        `UserAccount ${username}`,
        { userAccount }
      );
    });
    return response;
  }
}

module.exports = new UserAccountsServices();

const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const userRolesRepos = require("../repos/userRoles.repos");
const usersRepos = require("../repos/users.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class UserRolesServices {
  async registerUserRole(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(403, false, "registerRole", `Incorrect Password`);
      }
      const newUserRole = await userRolesRepos.create(reqBody, t);
      return responseObject(200, true, "registerRole", `UserRole Registered`, {
        newUserRole: _.omit(newUserRole.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllUserRoles() {
    const response = sequelize.transaction(async (t) => {
      const allUserRoles = await userRolesRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(200, true, "getAllUserRoles", "All UserRoles", {
        allUserRoles,
      });
    });
    return response;
  }

  async getUserRoleById(id) {
    const response = sequelize.transaction(async (t) => {
      const userRole = await userRolesRepos.findOne(
        {
          where: {
            id,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!userRole) {
        throw new ErrorClass(
          200,
          false,
          "getUserRoleById",
          `UserRole "${id}" Not found`
        );
      }
      return responseObject(200, true, "register", `UserRole "${id}"`, {
        userRole,
      });
    });
    return response;
  }

  async updateUserRoleById(reqUser, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateUserRoleById",
          `Incorrect Password`
        );
      }
      const userRole = await userRolesRepos.findOne(
        {
          where: {
            id,
          },
        },
        t
      );
      if (!userRole) {
        throw new ErrorClass(
          200,
          false,
          "updateUserRoleById",
          `UserRole "${id}" Not Found`
        );
      }
      await userRolesRepos.update(
        {
          username: reqBody.username || userRole.username,
          roleCode: reqBody.roleCode || userRole.roleCode,
        },
        userRole,
        t
      );
      return responseObject(200, true, "register", `UserRole "${id}" Updated`, {
        updatedUserRole: _.omit(userRole.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async deleteUserRoleById(reqUser, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deleteUserRoleById",
          `Incorrect Password`
        );
      }
      const userRole = await userRolesRepos.findOne({ where: { id } }, t);
      if (!userRole) {
        throw new ErrorClass(
          403,
          false,
          "deleteUserRoleById",
          `UserRole "${id}" Not Found`
        );
      }
      await userRolesRepos.delete(userRole, t);
      return responseObject(
        200,
        true,
        "deleteUserRoleById",
        `UserRole "${id}" Deleted.`,
        null
      );
    });
    return response;
  }

  async getAllUserRolesByUsername(username) {
    const response = sequelize.transaction(async (t) => {
      const user = await usersRepos.findOne({ where: { username } }, t);
      if (!user) {
        throw new ErrorClass(
          403,
          false,
          "getAllUserRolesByUsername",
          `User "${username}" Not Found`
        );
      }
      const allUserRoles = await userRolesRepos.findAll(
        {
          where: {
            username,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(200, true, "register", `UserRole "${username}"`, {
        allUserRoles,
      });
    });
    return response;
  }
}

module.exports = new UserRolesServices();

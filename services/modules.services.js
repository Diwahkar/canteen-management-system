const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const modulesRepos = require("../repos/modules.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class ModulesServices {
  async registerModule(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "registerModule",
          `Incorrect Password`
        );
      }
      const newModule = await modulesRepos.create(reqBody, t);
      return responseObject(200, true, "registerModule", `Module Registered`, {
        newModule: _.omit(newModule.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllModules() {
    const response = sequelize.transaction(async (t) => {
      const allModules = await modulesRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(200, true, "getAllModule", "All Modules", {
        allModules,
      });
    });
    return response;
  }

  async getModuleByCode(code) {
    const response = sequelize.transaction(async (t) => {
      const module = await modulesRepos.findOne(
        {
          where: {
            code,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!module) {
        throw new ErrorClass(
          404,
          false,
          "updateModuleByCode",
          `Module "${code}" Not Found`
        );
      }
      return responseObject(200, true, "register", `Module "${code}"`, {
        module,
      });
    });
    return response;
  }

  async updateModuleByCode(reqUser, code, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateModuleByCode",
          `Incorrect Password`
        );
      }
      const module = await modulesRepos.findOne({
        where: {
          code,
        },
      });
      if (!module) {
        throw new ErrorClass(
          404,
          false,
          "updateModuleByCode",
          `Module "${code}" Not Found`
        );
      }
      await modulesRepos.update(
        {
          desc: reqBody.desc,
        },
        module,
        t
      );
      return responseObject(
        200,
        true,
        "updateModuleByCode",
        `Module "${code}" Updated`,
        {
          updatedModule: _.omit(module.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deleteModuleByCode(reqUser, code, reqBody) {
    const passwordMatch = await compare(reqBody.password, reqUser.password);
    if (!passwordMatch) {
      throw new ErrorClass(
        403,
        false,
        "deleteModuleByCode",
        `Incorrect Password`
      );
    }
    const module = await modulesRepos.findOne({
      where: { code },
    });
    if (!module) {
      throw new ErrorClass(
        404,
        false,
        "deleteModuleByCode",
        `Module "${code}" Not Found`
      );
    }
    await module.destroy();
    return responseObject(
      200,
      true,
      "deleteModuleByCode",
      `Module "${code}" Deleted.`,
      null
    );
  }
}

module.exports = new ModulesServices();

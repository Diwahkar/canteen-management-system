const { sequelize } = require("../models");
const orderedItemsRepos = require("../repos/orderedItems.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");

class OrderedItemsServices {
  async getAllOrderedItems() {
    const response = sequelize.transaction(async (t) => {
      const allOrderedItems = await orderedItemsRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(
        200,
        true,
        "getAllOrderedItems",
        "All OrderedItems",
        { allOrderedItems }
      );
    });
    return response;
  }

  async getOrderedItemById(id) {
    const response = sequelize.transaction(async (t) => {
      const orderedItem = await orderedItemsRepos.findOne(
        {
          where: {
            id,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!orderedItem) {
        throw new ErrorClass(
          404,
          false,
          "getOrderedItemById",
          `OrderedItem "${id}" Not Found`
        );
      }
      console.log(orderedItem);
      return responseObject(200, true, "register", `OrderedItem "${id}"`, {
        orderedItem,
      });
    });
    return response;
  }

  async getAllOrderedItemsByOrderId(orderId) {
    const response = sequelize.transaction(async (t) => {
      const allOrderedItems = await orderedItemsRepos.findAll(
        {
          where: {
            orderId,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllOrderedItemsByOrderId",
        `OrderedItem ${orderId}`,
        { allOrderedItems }
      );
    });
    return response;
  }
}

module.exports = new OrderedItemsServices();

const orderService = require("./orders.services");
const userService = require("../services/users.services");

const Joi = require("joi");
const jwtDecode = require("jwt-decode");
const jwt = require("jsonwebtoken");
const jwtSecretsRepos = require("../repos/jwtSecrets.repos");
const usersRepos = require("../repos/users.repos");
const ErrorClass = require("../utils/custom.error");

const EventEmitter = require("events");
class SocketEmitter extends EventEmitter {}

const orderSchema = Joi.object({
  orderedItemsArr: Joi.array()
    .items(
      Joi.object({
        itemName: Joi.string().required(),
        quantity: Joi.number().required(),
      }).required()
    )
    .required(),
  pin: Joi.string().required(),
});

class SocketIOServices {
  socketEmitterInstance = new SocketEmitter();
  constructor() {
    this.func = () => {
      orderService.emitterInstance.on(
        "orderIsReady",
        ({ orderId, username }) => {
          this.socketEmitterInstance.emit("orderReady", { orderId, username });
        }
      );
    };
  }

  async loginUser(userCredenials) {
    const response = await userService.loginUser(userCredenials);
    if (response.success) {
      const token = response.token;
      const unVerifiedPayload = jwtDecode(token);
      const jwtSecret = await jwtSecretsRepos.findOne({
        where: { username: unVerifiedPayload.sub },
      });
      if (!jwtSecret) {
        throw new ErrorClass(
          401,
          false,
          "authentication",
          "Access Denied: Pending Sign Up Or Email Verification"
        );
      }
      const verifiedPayload = jwt.verify(token, jwtSecret.key);
      return await usersRepos.findOne({
        where: { username: verifiedPayload.sub },
      });
    }
  }

  async postOrder(user, data, socket) {
    const reqBody = JSON.parse(data);
    const { value, error } = orderSchema.validate(reqBody);
    if (error)
      throw new ErrorClass(
        422,
        false,
        "JoiValidation",
        error.details[0].message
      );
    const response = await orderService.registerOrder(user, reqBody);
    return socket.emit("orderReceived", {
      origin: "orderReceived",
      text: `Order Received. Order id is ${response.newOrder.id}`,
      createdAt: new Date().toLocaleString(),
    });
  }

  postOrderError(error, socket) {
    return socket.emit("postOrderError", {
      origin: "postOrderError",
      text: `${error.message}`,
      createdAt: new Date().toLocaleString(),
    });
  }

  async orderStatus(user, orderId, socket) {
    const response = await orderService.getOrderById(user, orderId);
    if (response.order === null) {
      return socket.emit("orderStatusError", {
        origin: "orderStatusError",
        text: response.message,
        createdAt: new Date().toLocaleString(),
      });
    }
    return socket.emit("orderStatusResponse", {
      origin: "orderStatusResponse",
      text: `Order ${response.order.id} is ` + `${response.order.status}`,
      createdAt: new Date().toLocaleString(),
    });
  }

  orderStatusError(error, socket) {
    return socket.emit("orderStatusError", {
      origin: "orderStatusError",
      text: error.message,
      createdAt: new Date().toLocaleString(),
    });
  }

  emitter(data, socket) {
    return socket.to(data.username).emit("ready", {
      origin: "ready",
      text: `Order ${data.orderId} is Ready`,
      createdAt: new Date().toLocaleString(),
    });
  }

  emitterError(error, socket) {
    return socket.emit("emitterError", {
      origin: "emitterError",
      text: error.message,
      createdAt: new Date().toLocaleString(),
    });
  }
}

module.exports = new SocketIOServices();

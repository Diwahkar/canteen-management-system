const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const modulePrivilegesRepos = require("../repos/modulePrivileges.repos");
const modulesRepos = require("../repos/modules.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class ModulePrivilegeServices {
  async registerModulePrivilege(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "registerModulePrivilege",
          `Incorrect Password`
        );
      }
      const newModulePrivilege = await modulePrivilegesRepos.create(reqBody, t);
      return responseObject(
        200,
        true,
        "registerModulePrivilege",
        `ModulePrivilege Registered`,
        {
          newModulePrivilege: _.omit(newModulePrivilege.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async getAllModulePrivileges() {
    const response = sequelize.transaction(async (t) => {
      const allModulePrivileges = await modulePrivilegesRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(
        200,
        true,
        "getAllModulePrivileges",
        "All ModulePrivileges",
        { allModulePrivileges }
      );
    });
    return response;
  }

  async getModulePrivilegeById(id) {
    const response = sequelize.transaction(async (t) => {
      const modulePrivilege = await modulePrivilegesRepos.findOne(
        {
          where: {
            id,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!modulePrivilege) {
        throw new ErrorClass(
          404,
          false,
          "getModulePrivilegeById",
          `ModulePrivilege "${id}" Not Found`
        );
      }
      return responseObject(
        200,
        true,
        "getModulePrivilegeById",
        `ModulePrivilege "${id}"`,
        { modulePrivilege }
      );
    });
    return response;
  }

  async updateModulePrivilegeById(admin, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateModulePrivilegeById",
          `Incorrect Password`
        );
      }
      const modulePrivilege = await modulePrivilegesRepos.findOne(
        {
          where: {
            id,
          },
        },
        t
      );
      if (!modulePrivilege) {
        throw new ErrorClass(
          404,
          false,
          "updateModulePrivilegeById",
          `ModulePrivilege "${id}" Not Found`
        );
      }
      await modulePrivilegesRepos.update(
        {
          moduleCode: reqBody.moduleCode || modulePrivilege.moduleCode,
          privilegeCode: reqBody.privilegeCode || modulePrivilege.privilegeCode,
        },
        modulePrivilege,
        t
      );
      return responseObject(
        200,
        true,
        "updateModulePrivilegeById",
        `ModulePrivilege "${id}" Updated`,
        {
          updatedModulePrivilege: _.omit(modulePrivilege.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deleteModulePrivilegeById(admin, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deleteModulePrivilegeById",
          `Incorrect Password`
        );
      }
      const modulePrivilege = await modulePrivilegesRepos.findOne(
        {
          where: { id },
        },
        t
      );
      if (!modulePrivilege) {
        throw new ErrorClass(
          404,
          false,
          "deleteModulePrivilegeById",
          `ModulePrivilege "${id}" Not Found`
        );
      }
      await modulePrivilegesRepos.delete(modulePrivilege, t);
      return responseObject(
        200,
        true,
        "deleteModulePrivilegeById",
        `ModulePrivilege "${id}" Deleted.`,
        null
      );
    });
    return response;
  }

  async getAllModulePrivilegesByModule(moduleCode) {
    const response = sequelize.transaction(async (t) => {
      const module = await modulesRepos.findOne(
        {
          where: { code: moduleCode },
        },
        t
      );
      if (!module) {
        throw new ErrorClass(
          404,
          false,
          "getAllModulePrivilegeByModule",
          `Module ${moduleCode} Not Found`
        );
      }
      const allModulePrivileges = await modulePrivilegesRepos.findAll(
        {
          where: {
            moduleCode,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllModulePrivilegesByModule",
        `ModulePrivilege with ModuleCode "${moduleCode}"`,
        { allModulePrivileges }
      );
    });
    return response;
  }

  async getAllModulePrivilegesByPrivilege(privilegeCode) {
    const response = sequelize.transaction(async (t) => {
      const privilege = await privilegesRepos.findOne(
        {
          where: { code: privilegeCode },
        },
        t
      );
      if (!privilege) {
        throw new ErrorClass(
          404,
          false,
          "getAllModulePrivilegesByModule",
          `Module ${moduleCode} Not Found`
        );
      }
      const allModulePrivileges = await modulePrivilegesRepos.findAll(
        {
          where: {
            privilegeCode,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllModulePrivilegesByPrivilege",
        `ModulePrivilege with Privilege Code ${privilegeCode}`,
        { allModulePrivileges }
      );
    });
    return response;
  }
}

module.exports = new ModulePrivilegeServices();

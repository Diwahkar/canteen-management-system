const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const categoriesRepos = require("../repos/categories.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class CategoriesServices {
  async registerCategory(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "registerCategory",
          `Incorrect Password`
        );
      }
      const newCategory = await categoriesRepos.create(reqBody, t);
      return responseObject(
        200,
        true,
        "registerCategory",
        `Category Registered`,
        {
          newCategory: _.omit(newCategory.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async getAllCategories() {
    const response = sequelize.transaction(async (t) => {
      const allCategories = await categoriesRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(200, true, "getAllCategories", "All Categories", {
        allCategories,
      });
    });
    return response;
  }

  async getCategoryByName(name) {
    const response = sequelize.transaction(async (t) => {
      const category = await categoriesRepos.findOne(
        {
          where: {
            name,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },

          include: "items",
        },
        t
      );
      if (!category) {
        throw new ErrorClass(
          404,
          false,
          "getCategoryByName",
          `Category "${name}" Not Found`
        );
      }
      return responseObject(
        200,
        true,
        "getCategoryByName",
        `Category "${name}"`,
        { category }
      );
    });
    return response;
  }

  async updateCategoryByName(reqUser, name, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateCategoryByName",
          `Incorrect Password`
        );
      }
      const category = await categoriesRepos.findOne(
        {
          where: {
            name,
          },
        },
        t
      );
      if (!category) {
        throw new ErrorClass(
          404,
          false,
          "updateCategoryByName",
          `Category "${name}" Not Found`
        );
      }
      await categoriesRepos.update(
        {
          name: reqBody.name || category.name,
        },
        category,
        t
      );
      return responseObject(
        200,
        true,
        "updateCategoryByName",
        `Category ${category.id} Updated`,
        {
          updatedCategory: _.omit(category.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deleteCategoryByName(reqUser, name, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deleteCategoryByName",
          `Incorrect Password`
        );
      }
      const category = await categoriesRepos.findOne({ where: { name } }, t);
      if (!category) {
        throw new ErrorClass(
          404,
          false,
          "deleteCategoryByName",
          `Category "${name}" Not Found`
        );
      }
      await categoriesRepos.delete(category, t);
      return responseObject(
        200,
        true,
        "deleteCategoryByName",
        `Category "${name}" Deleted.`,
        null
      );
    });
    return response;
  }
}

module.exports = new CategoriesServices();

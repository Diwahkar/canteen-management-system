const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const findRoles = require("../utils/find.roles");
const ordersRepos = require("../repos/orders.repos");
const paymentsRepos = require("../repos/payments.repos");
const transactionsRepos = require("../repos/transactions.repos");
const userAccountsRepos = require("../repos/userAccounts.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class PaymentsServices {
  async registerPayment(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "registerPayment",
          `Incorrect Password`
        );
      }

      const order = await ordersRepos.findOne(
        { where: { id: reqBody.orderId } },
        t
      );
      if (!order) {
        throw new ErrorClass(
          403,
          false,
          "registerPayment",
          `Order ${reqBody.orderId} Not Found`
        );
      }
      reqBody.amount = order.total;
      reqBody.username = order.username;
      const payment = await paymentsRepos.findOne(
        {
          where: {
            orderId: order.id,
          },
        },
        t
      );
      if (payment) {
        throw new ErrorClass(
          403,
          false,
          "registerPayment",
          `Order ${order.id} is Already Paid`
        );
      }
      const newPayment = await paymentsRepos.create(reqBody, t);
      await ordersRepos.update(
        {
          paid: true,
          paymentId: newPayment.id,
        },
        order,
        t
      );

      let deposit = 0,
        withdrawn = 0;
      if (
        newPayment.amount >
        newPayment.amountReceived - newPayment.amountReturned
      )
        withdrawn =
          newPayment.amount +
          newPayment.amountReturned -
          newPayment.amountReceived;
      else if (
        newPayment.amount <
        newPayment.amountReceived - newPayment.amountReturned
      )
        deposit =
          newPayment.amountReceived -
          newPayment.amount -
          newPayment.amountReturned;

      const userAccount = await userAccountsRepos.findOne(
        {
          where: {
            username: newPayment.username,
          },
        },
        t
      );
      await userAccountsRepos.update(
        {
          balance: userAccount.balance + deposit - withdrawn,
        },
        userAccount,
        t
      );
      const newTransaction = await transactionsRepos.create(
        {
          deposit,
          withdrawn,
          paymentId: newPayment.id,
          username: newPayment.username,
          onHandBalance: userAccount.balance,
        },
        t
      );
      await paymentsRepos.update(
        {
          transactionId: newTransaction.id,
        },
        newPayment,
        t
      );
      return responseObject(
        200,
        true,
        "registerPayment",
        `Payment Registered`,
        {
          newPayment: _.omit(newPayment.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async getAllPayments(reqUser) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier) {
        const allPayments = await paymentsRepos.findAll(
          { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
          t
        );
        return responseObject(200, true, "getAllPayments", "All Payments", {
          allPayments,
        });
      }
      const allMyPayments = await paymentsRepos.findAll(
        {
          where: { username: reqUser.username },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(200, true, "getAllPayments", "All Payments", {
        allMyPayments,
      });
    });
    return response;
  }

  async getPaymentById(reqUser, id) {
    const response = sequelize.transaction(async (t) => {
      const payment = await paymentsRepos.findOne(
        {
          where: {
            id,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!payment) {
        throw new ErrorClass(
          403,
          false,
          "getPaymentById",
          `Payment "${id}" Not found`
        );
      }
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier || reqUser.username === payment.username) {
        return responseObject(200, true, "getPaymentById", `Payment "${id}"`, {
          payment,
        });
      }
      throw new ErrorClass(403, false, "getPaymentById", "Access Denied");
    });
    return response;
  }

  async getAllPaymentsByUsername(reqUser, username) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier || reqUser.username === payment.username) {
        const allPayments = await paymentsRepos.findAll(
          {
            where: {
              username,
            },
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
          t
        );
        return responseObject(
          200,
          true,
          "getAllPaymentsByUsername",
          `Payments "${username}"`,
          { allPayments }
        );
      }
      throw new ErrorClass(
        403,
        false,
        "getAllPaymentsByUsername",
        "Access Denied"
      );
    });
    return response;
  }
}

module.exports = new PaymentsServices();

const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const privilegesRepos = require("../repos/privileges.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class PrivilegesServices {
  async registerPrivilege(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "registerPrivilege",
          `Incorrect Password`
        );
      }
      const newPrivilege = await privilegesRepos.create(reqBody, t);
      return responseObject(
        200,
        true,
        "registerPrivilege",
        `Privilege Registered`,
        {
          newPrivilege: _.omit(newPrivilege.dataValues, [
            "createdAt",
            "updatedAt",
          ]),
        }
      );
    });
    return response;
  }

  async getAllPrivileges() {
    const response = sequelize.transaction(async (t) => {
      const allPrivileges = await privilegesRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(200, true, "getAllPrivileges", "All Privileges", {
        allPrivileges,
      });
    });
    return response;
  }

  async getPrivilegeByCode(code) {
    const response = sequelize.transaction(async (t) => {
      const privilege = await privilegesRepos.findOne(
        {
          where: {
            code,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!privilege) {
        throw new ErrorClass(
          403,
          false,
          "getPrivilegeByCode",
          `Privilege "${code}" Not found`
        );
      }
      return responseObject(
        200,
        true,
        "getPrivilegeByCode",
        `Privilege "${code}"`,
        { privilege }
      );
    });
    return response;
  }

  async updatePrivilegeByCode(admin, code, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const privilege = await privilegesRepos.findOne(
        {
          where: {
            code,
          },
        },
        t
      );
      if (!privilege) {
        throw new ErrorClass(
          403,
          false,
          "updatePrivilegeByCode",
          `Privilege "${code}" Not found`
        );
      }
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updatePrivilegeByCode",
          `Incorrect Password`
        );
      }
      await privilegesRepos.update(
        {
          code: reqBody.code,
          desc: reqBody.desc,
        },
        privilege,
        t
      );
      return responseObject(
        200,
        true,
        "updatePrivilegeByCode",
        `Privilege "${code}" Updated`,
        {
          updatedPrivilege: _.omit(privilege.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deletePrivilegeByCode(admin, code, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deletePrivilegeByCode",
          `Incorrect Password`
        );
      }
      const privilege = await privilegesRepos.findOne({ where: { code } }, t);
      if (!privilege) {
        throw new ErrorClass(
          403,
          false,
          "deletePrivilegeByCode",
          `Privilege "${code}" Not found`
        );
      }
      await privilegesRepos.delete(privilege, t);
      return responseObject(
        200,
        true,
        "deletePrivilegeByCode",
        `Privilege "${code}" Deleted.`,
        null
      );
    });
    return response;
  }
}

module.exports = new PrivilegesServices();

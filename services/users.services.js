const { sequelize } = require("../models");
const { hash, compare } = require("../utils/hash");
const generateToken = require("../utils/token");
const nodeMailer = require("../utils/nodemailer");
const usernameOrEmail = require("../utils/input.is.username.or.email");
const obscureEmail = require("../utils/obscureEmail");
const findRoles = require("../utils/find.roles");
const usersRepos = require("../repos/users.repos");
const userAccountsRepos = require("../repos/userAccounts.repos");
const userRolesRepos = require("../repos/userRoles.repos");
const cloudinary = require("../utils/cloudinary");
const jwtSecretsRepos = require("../repos/jwtSecrets.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const crypto = require("crypto");
const _ = require("lodash");

class UsersServices {
  async registerUser(reqBody) {
    const response = await sequelize.transaction(async (t) => {
      const emailVerifyCode = crypto.randomBytes(4).toString("hex");

      reqBody.username = reqBody.username.toLowerCase();
      reqBody.email = reqBody.email.toLowerCase();
      reqBody.password = await hash(reqBody.password);
      reqBody.pin = await hash(reqBody.pin);
      reqBody.emailVerifyCode = await hash(emailVerifyCode);

      const image = await cloudinary.uploader.upload(reqBody.displayPic);
      reqBody.displayPic = image.url;

      const mailObject = {
        receiverEmail: reqBody.email,
        subject: "Email Verification",
        emailVerifyCode,
      };

      await usersRepos.create(reqBody, t);
      await nodeMailer(mailObject);
      return responseObject(200, true, "register", "Profile Created", {
        remark: `Please, Check Your Email ${obscureEmail(
          reqBody.email
        )} for Account Verification`,
      });
    });
    return response;
  }

  async getAllUsers(reqUser) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier) {
        const allUsers = await usersRepos.findAll(
          {
            attributes: [
              "id",
              "name",
              "username",
              "email",
              "department",
              "position",
              "displayPic",
            ],
          },
          t
        );
        return responseObject(200, true, "getAllUsers", "All users", {
          allUsers,
        });
      }
      return this.getMyProfile(reqUser);
    });
    return response;
  }
  async getMyProfile(reqUser) {
    return responseObject(
      200,
      true,
      "getMyProfile",
      `User "${reqUser.username}"`,
      {
        myProfile: _.pick(reqUser, [
          "name",
          "username",
          "email",
          "department",
          "position",
          "displayPic",
        ]),
      }
    );
  }

  async updateMyProfile(reqUser, reqBody) {
    const response = await sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateMyProfile",
          `Incorrect Password`
        );
      }
      if (reqBody.displayPic) {
        const oldImage = reqUser.displayPic;
        const imageId = oldImage.split("/").pop().split(".")[0];
        await cloudinary.uploader.destroy(imageId);
        const image = await cloudinary.uploader.upload(reqBody.displayPic);
        reqBody.displayPic = image.url;
      }

      await usersRepos.update(
        {
          name: reqBody.name || reqUser.name,
          username: reqBody.username?.toLowerCase() || reqUser.username,
          email: reqBody.email?.toLowerCase() || reqUser.email,
          password: reqBody.newPassword
            ? await hash(reqBody.newPassword, 10)
            : reqUser.password,
          pin: reqBody.pin ? await hash(reqBody.pin, 10) : reqUser.pin,
          department: reqBody.department || reqUser.department,
          displayPic: reqBody.displayPic || reqUser.displayPic,
        },
        reqUser,
        t
      );
      if (reqBody.newPassword) {
        const jwtSecret = await jwtSecretsRepos.findOne({
          where: { username: reqUser.username },
        });
        await jwtSecretsRepos.update(
          {
            key: crypto.randomBytes(8).toString("hex"),
          },
          jwtSecret,
          t
        );
        const token = generateToken(reqUser.username, jwtSecret.key);
        return responseObject(
          200,
          true,
          "loginUser",
          "Password Changed Successfully",
          { token }
        );
      }
      return responseObject(200, true, "updateMyProfile", "Profile Updated", {
        updatedProfile: _.pick(reqUser, [
          "name",
          "username",
          "email",
          "department",
          "position",
          "displayPic",
        ]),
      });
    });
    return response;
  }

  async deleteMyProfile(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deleteMyProfile",
          `Incorrect Password`
        );
      }
      const { isAdmin } = await findRoles(reqUser.username);
      if (isAdmin) {
        throw new ErrorClass(
          403,
          false,
          "deleteUserByUsername",
          `Can't Delete Admin's Profile`
        );
      }
      const userAccount = await userAccountsRepos.findOne(
        {
          where: {
            username: reqUser.username,
          },
        },
        { transaction: t }
      );
      if (!userAccount) {
        throw new ErrorClass(
          404,
          false,
          "deleteMyProfile",
          `User "${reqUser.username}" Not Found`
        );
      }
      if (userAccount.balance !== 0) {
        throw new ErrorClass(
          403,
          false,
          "deleteMyProfile",
          "Balance Must Be 0 for Profile Deletion"
        );
      }
      await usersRepos.delete(reqUser, t);
      const userDisplayPic = reqUser.displayPic;
      const imageId = userDisplayPic.split("/").pop().split(".")[0];
      await cloudinary.uploader.destroy(imageId);
      return responseObject(
        200,
        true,
        "deleteMyProfile",
        `User "${reqUser.username}" Deleted`,
        null
      );
    });
    return response;
  }

  async emailVerify(reqBody) {
    const response = sequelize.transaction(async (t) => {
      let user;
      const { username, email } = usernameOrEmail(
        reqBody.usernameOrEmail.toLowerCase()
      );
      if (username) {
        reqBody.username = username;
        user = await usersRepos.findOne({ where: { username } }, t);
      } else {
        reqBody.email = email;
        user = await usersRepos.findOne({ where: { email } }, t);
      }
      if (!user) {
        throw new ErrorClass(404, false, "emailVerify", "Invalid Credentials");
      }
      if (user.emailVerified) {
        throw new ErrorClass(
          401,
          false,
          "emailVerify",
          `Email is Already Verified`
        );
      }
      const emailVerifyCodeMatch = await compare(
        reqBody.emailVerifyCode,
        user.emailVerifyCode
      );
      if (!emailVerifyCodeMatch) {
        throw new ErrorClass(401, false, "emailVerify", "Invalid Credentials");
      }
      await usersRepos.update(
        {
          emailVerified: true,
          emailVerifyCode: null,
        },
        user,
        t
      );
      await userAccountsRepos.create(
        {
          username: user.username,
          creditLimit:
            user.position === "senior"
              ? 15000
              : user.position === "mid"
              ? 10000
              : 5000,
        },
        t
      );
      await userRolesRepos.create(
        {
          username: user.username,
        },
        t
      );
      await jwtSecretsRepos.create({
        username: user.username,
        key: crypto.randomBytes(8).toString("hex"),
      });
      return responseObject(
        200,
        true,
        "emailVerify",
        "Email Verified Successfully, May Proceed to Login"
      );
    });
    return response;
  }

  async loginUser(reqBody) {
    const response = sequelize.transaction(async (t) => {
      let user;
      const { username, email } = usernameOrEmail(
        reqBody.usernameOrEmail.toLowerCase()
      );
      if (username) {
        reqBody.username = username;
        user = await usersRepos.findOne({ where: { username } }, t);
      } else {
        reqBody.email = email;
        user = await usersRepos.findOne({ where: { email } }, t);
      }
      if (!user) {
        throw new ErrorClass(404, false, "loginUser", "Invalid Credentials");
      }
      if (!user.emailVerified) {
        throw new ErrorClass(
          401,
          false,
          "loginUser",
          `Please, Verify Your Email First`
        );
      }
      const passwordMatch = await compare(reqBody.password, user.password);
      if (!passwordMatch) {
        throw new ErrorClass(403, false, "loginUser", `Invalid Credentials`);
      }
      if (user.forgotPassword) {
        await usersRepos.update(
          {
            forgotPassword: false,
            resetForgottenPasswordCode: null,
          },
          user,
          t
        );
      }
      const jwtSecret = await jwtSecretsRepos.findOne(
        { where: { username: user.username } },
        t
      );
      const token = generateToken(user.username, jwtSecret.key);
      return responseObject(200, true, "loginUser", "Logged in successfully.", {
        token,
      });
    });
    return response;
  }

  async forgotPassword(reqBody) {
    const response = sequelize.transaction(async (t) => {
      let user;
      const { username, email } = usernameOrEmail(
        reqBody.usernameOrEmail.toLowerCase()
      );
      if (username) {
        reqBody.username = username;
        user = await usersRepos.findOne({ where: { username } }, t);
      } else {
        reqBody.email = email;
        user = await usersRepos.findOne({ where: { email } }, t);
      }
      if (!user) {
        throw new ErrorClass(
          404,
          false,
          "loginUser",
          `${
            username
              ? 'username: "' + reqBody.username
              : 'email: "' + reqBody.email
          }" Not Found`
        );
      }
      if (!user.emailVerified) {
        throw new ErrorClass(
          401,
          false,
          "loginUser",
          `Please, Verify Your Email First`
        );
      }
      const resetForgottenPasswordCode = crypto.randomBytes(4).toString("hex");
      const mailObject = {
        receiverEmail: user.email,
        subject: "Reset Password",
        resetForgottenPasswordCode,
      };
      await nodeMailer(mailObject);
      await usersRepos.update(
        {
          forgotPassword: true,
          resetForgottenPasswordCode: await hash(resetForgottenPasswordCode),
        },
        user,
        t
      );

      return responseObject(
        200,
        true,
        "forgotPassword",
        "Forgot Password Acknowledged",
        {
          remark: `Please, Check Your Email ${obscureEmail(
            user.email
          )} to Reset Password`,
        }
      );
    });
    return response;
  }

  async resetForgottenPassword(reqBody) {
    const response = sequelize.transaction(async (t) => {
      let user;
      const { username, email } = usernameOrEmail(
        reqBody.usernameOrEmail.toLowerCase()
      );
      if (username) {
        reqBody.username = username;
        user = await usersRepos.findOne({ where: { username } }, t);
      } else {
        reqBody.email = email;
        user = await usersRepos.findOne({ where: { email } }, t);
      }
      if (!user) {
        throw new ErrorClass(
          404,
          false,
          "resetForgottenPassword",
          "Invalid Credentials"
        );
      }
      if (!user.forgotPassword) {
        throw new ErrorClass(
          401,
          false,
          "resetForgottenPassword",
          `Password is Already Set`
        );
      }
      const resetForgottenPasswordCodeMatch = await compare(
        reqBody.resetForgottenPasswordCode,
        user.resetForgottenPasswordCode
      );
      if (!resetForgottenPasswordCodeMatch) {
        throw new ErrorClass(401, false, "emailVerify", "Invalid Credentials");
      }
      reqBody.newPassword = await hash(reqBody.newPassword);
      await usersRepos.update(
        {
          password: reqBody.newPassword,
          forgotPassword: false,
          resetForgottenPasswordCode: null,
        },
        user,
        t
      );
      const jwtSecret = await jwtSecretsRepos.findOne({
        where: { username: user.username },
      });
      await jwtSecretsRepos.update(
        {
          key: crypto.randomBytes(8).toString("hex"),
        },
        jwtSecret,
        t
      );

      return responseObject(
        200,
        true,
        "resetForgottenPassword",
        "Password Reset Successfully, May Proceed to Login"
      );
    });
    return response;
  }

  async getUserByUsername(username) {
    const response = sequelize.transaction(async (t) => {
      const user = await usersRepos.findOne(
        {
          where: {
            username,
          },
          attributes: [
            "id",
            "name",
            "username",
            "email",
            "department",
            "position",
            "displayPic",
          ],
        },
        t
      );
      if (!user) {
        throw new ErrorClass(
          404,
          false,
          "getUserByUsername",
          `User "${username}" Not Found`
        );
      }
      return responseObject(
        200,
        true,
        "getUserByUsername",
        `User "${username}"`,
        { user }
      );
    });
    return response;
  }

  async updateUserByUsername(reqUser, username, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateUserByUsername",
          `Incorrect Password`
        );
      }
      const user = await usersRepos.findOne(
        {
          where: {
            username,
          },
        },
        t
      );

      await usersRepos.update(
        {
          position: reqBody.position,
        },
        user,
        t
      );
      const userAccount = await userAccountsRepos.findOne(
        {
          where: {
            username: user.username,
          },
        },
        t
      );

      await userAccountsRepos.update(
        {
          creditLimit:
            user.position === "senior"
              ? 15000
              : user.position === "mid"
              ? 10000
              : 5000,
        },
        userAccount,
        t
      );
      return responseObject(
        200,
        true,
        "updateUserByUsername",
        `${user.username}'s Position Updated`,
        { userPosition: user.position }
      );
    });
    return response;
  }

  async deleteUserByUsername(reqUser, username, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        return responseObject(
          200,
          false,
          "deleteUserByUsername",
          `Incorrect Password`,
          null
        );
      }
      const { isAdmin } = await findRoles(username);
      if (isAdmin) {
        throw new ErrorClass(
          403,
          false,
          "deleteUserByUsername",
          `Can't Delete Admin's Profile`
        );
      }
      const userAccount = await userAccountsRepos.findOne(
        {
          where: {
            username,
          },
        },
        t
      );
      if (!userAccount) {
        throw new ErrorClass(
          404,
          false,
          "deleteUserByUsername",
          `User "${username}" Not Found`
        );
      }
      if (userAccount.balance !== 0) {
        throw new ErrorClass(
          403,
          false,
          "deleteUserByUsername",
          "Balance Must Be 0 for Profile Deletion"
        );
      }
      const user = await usersRepos.findOne(
        {
          where: {
            username,
          },
        },
        t
      );
      await usersRepos.delete(user, t);
      return responseObject(
        200,
        true,
        "deleteUserByUsername",
        `User "${username}" Deleted`,
        null
      );
    });
    return response;
  }

  // async deleteUsersWithUnverfiedEmails() {
  //   const response = sequelize.transaction(async (t) => {
  //     await sequelize.query(
  //       `
  //         DELETE FROM "users"
  //         WHERE
  //           "emailVerified"='false'
  //         AND
  //           "createdAt"<=clock_timestamp() - interval '2 days';
  //      `
  //     );
  //     // await sequelize.query(
  //     //   `
  //     //     DELETE FROM "users"
  //     //     WHERE
  //     //       "emailVerified"='false'
  //     //     AND
  //     //       "createdAt"<=clock_timestamp() - interval '1 minutes';
  //     //  `
  //     // )
  // });
  // }
}

module.exports = new UsersServices();

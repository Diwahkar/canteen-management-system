const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const routesRepos = require("../repos/routes.repos");
const privilegesRepos = require("../repos/privileges.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class RoutesServices {
  async registerRoute(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(403, false, "registerRoute", `Incorrect Password`);
      }
      const newRoute = await routesRepos.create(reqBody, t);
      return responseObject(200, true, "register", `Route Registered`, {
        newRoute: _.omit(newRoute.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllRoutes() {
    const response = sequelize.transaction(async (t) => {
      const allRoutes = await routesRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(200, true, "getAllRoutes", "All Routes", {
        allRoutes,
      });
    });
    return response;
  }

  async getRouteById(id) {
    const response = sequelize.transaction(async (t) => {
      const route = await routesRepos.findOne(
        {
          where: {
            id,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!route) {
        throw new ErrorClass(
          404,
          false,
          "getRouteById",
          `Route "${id}" Not found`
        );
      }
      return responseObject(200, true, "getRouteById", `Route "${id}"`, {
        route,
      });
    });
    return response;
  }

  async updateRouteById(admin, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateRouteById",
          `Incorrect Password`
        );
      }
      const route = await routesRepos.findOne(
        {
          where: {
            id,
          },
        },
        t
      );
      if (!route) {
        throw new ErrorClass(
          404,
          false,
          "updateRouteById",
          `Route "${id}" Not Found`
        );
      }
      await routesRepos.update(
        {
          method: reqBody.method,
          url: reqBody.url,
          privilegeCode: reqBody.privilegeCode,
        },
        route,
        t
      );
      return responseObject(
        200,
        true,
        "updateRouteById",
        `Route "${id}" Updated`,
        { updatedRoute: _.omit(route, ["createdAt", "updatedAt", "deletedAt"]) }
      );
    });
    return response;
  }

  async deleteRouteById(admin, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deleteRouteById",
          `Incorrect Password`
        );
      }
      const route = await routesRepos.findOne({ where: { id } }, t);
      if (!route) {
        throw new ErrorClass(
          404,
          false,
          "deleteRouteById",
          `Route "${id}" Not Found`
        );
      }
      await routesRepos.delete(route, t);
      return responseObject(
        200,
        true,
        "deleteRouteById",
        `Route "${id}" Deleted.`,
        null
      );
    });
    return response;
  }

  async getAllRoutesByPrivilege(privilegeCode) {
    const response = sequelize.transaction(async (t) => {
      const privilege = await privilegesRepos.findOne({
        where: { code: privilegeCode },
      });
      if (!privilege) {
        throw new ErrorClass(
          404,
          false,
          "getAllRoutesByPrivilege",
          `Privilege "${privilegeCode}" Not Found`
        );
      }
      const allRoutes = await routesRepos.findAll(
        {
          where: {
            privilegeCode,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllRoutesByPrivilege",
        `Route ${privilegeCode}`,
        {
          allRoutes,
        }
      );
    });
    return response;
  }
}

module.exports = new RoutesServices();

const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const findRoles = require("../utils/find.roles");
const usersRepos = require("../repos/users.repos");
const userAccountsRepos = require("../repos/userAccounts.repos");
const responseObject = require("../utils/response.object");
const transactionsRepos = require("../repos/transactions.repos");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class TransactionsServices {
  async registerTransaction(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "registerTransaction",
          "Access Denied"
        );
      }
      const user = await usersRepos.findOne(
        { where: { username: reqBody.username } },
        t
      );
      if (!user) {
        throw new ErrorClass(
          404,
          false,
          "registerTransaction",
          `User ${reqBody.username} Not Found`
        );
      }
      const userAccount = await userAccountsRepos.findOne(
        { where: { username: user.username } },
        t
      );
      reqBody.onHandBalance = userAccount.balance + reqBody.deposit;
      const newTransaction = await transactionsRepos.create(reqBody, t);
      await userAccountsRepos.update(
        {
          balance: userAccount.balance + reqBody.deposit,
        },
        userAccount,
        t
      );
      return responseObject(200, true, "registerTransaction", {
        newTransaction: _.omit(newTransaction.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllTransactions(reqUser) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier) {
        const allTransactions = await transactionsRepos.findAll(
          { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
          t
        );
        return responseObject(
          200,
          true,
          "getAllTransactions",
          `All Transactions`,
          { allTransactions }
        );
      }
      const allTransactions = await transactionsRepos.findAll(
        {
          where: { username: reqUser.username },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllTransactions",
        `All Transactions`,
        { allTransactions }
      );
    });
    return response;
  }

  async getTransactionById(reqUser, id) {
    const response = sequelize.transaction(async (t) => {
      const transaction = await transactionsRepos.findOne(
        {
          where: { id },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!transaction) {
        throw new ErrorClass(
          403,
          false,
          "getTransactionById",
          `Transaction ${id} Not Found`
        );
      }
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier || reqUser.username === transaction.username) {
        return responseObject(
          200,
          true,
          "getTransactionById",
          `Transaction "${id}"`,
          { transaction }
        );
      }
      throw new ErrorClass(403, false, "getTransactionById", "Access Denied");
    });
    return response;
  }

  async getAllTransactionsByUsername(reqUser, username) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier || reqUser.username === username) {
        const user = await usersRepos.findOne({ where: { username } });
        if (!user) {
          throw new ErrorClass(
            404,
            false,
            "getAllTransactionsByUsername",
            `User "${username}" Not Found`
          );
        }
        const allTransactions = await transactionsRepos.findAll(
          {
            where: { username },
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
          t
        );
        return responseObject(
          200,
          true,
          "getTransactionByUsername",
          `Transaction "${username}"`,
          { allTransactions }
        );
      }
      throw new ErrorClass(
        403,
        false,
        "getTransactionByUsername",
        "Access Denied"
      );
    });
    return response;
  }
}

module.exports = new TransactionsServices();

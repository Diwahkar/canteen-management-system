const { orderedItems, sequelize } = require("../models");
const { compare } = require("../utils/hash");
const findRoles = require("../utils/find.roles");
const itemsRepos = require("../repos/items.repos");
const orderedItemsRepos = require("../repos/orderedItems.repos");
const ordersRepos = require("../repos/orders.repos");
const userAccountsRepos = require("../repos/userAccounts.repos");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");
const EventEmitter = require("events");
class Emitter extends EventEmitter {}

class OrdersServices {
  emitterInstance = new Emitter();

  async registerOrder(reqUser, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const matchPin = await compare(reqBody.pin, reqUser.pin);
      if (!matchPin) {
        throw new ErrorClass(
          403,
          false,
          "registerOrder",
          "Incorrect Pin Number"
        );
      }
      const unPaidOrder = await ordersRepos.findOne(
        {
          where: {
            username: reqUser.username,
            paid: false,
          },
        },
        t
      );
      if (unPaidOrder) {
        throw new ErrorClass(
          403,
          false,
          "registerOrder",
          `Order ${unPaidOrder.id} is still unpaid.`
        );
      }
      const { orderedItemsArr } = reqBody;
      for (let i = 0; i < orderedItemsArr.length; i++) {
        const item = await itemsRepos.findOne(
          { where: { name: orderedItemsArr[i].itemName } },
          t
        );
        if (item.availableQuantity < orderedItemsArr[i].quantity) {
          throw new ErrorClass(
            403,
            false,
            "registerOrder",
            `${orderedItemsArr[i].itemName} Out of Stock, Available Quantity: ${item.availableQuantity}`
          );
        }
        await itemsRepos.update(
          {
            soldQuantity: item.soldQuantity + orderedItemsArr[i].quantity,
            availableQuantity:
              item.availableQuantity - orderedItemsArr[i].quantity,
          },
          item,
          t
        );
        orderedItemsArr[i].total = item.price * orderedItemsArr[i].quantity;
      }
      const userAccount = await userAccountsRepos.findOne(
        { where: { username: reqUser.username } },
        t
      );
      let total = orderedItemsArr
        .map((orderedItem) => orderedItem.total)
        .reduce(
          (partialTotal, orderedItemTotal) => partialTotal + orderedItemTotal,
          0
        );
      if (Math.abs(userAccount.balance - total) > userAccount.creditLimit) {
        throw new ErrorClass(
          403,
          false,
          "registerOrder",
          "Order-Amount Excedes Credit Limit"
        );
      }
      const newOrder = await ordersRepos.create(
        {
          total: total,
          username: reqUser.username,
        },
        t
      );
      orderedItemsArr.forEach(
        (orderedItem) => (orderedItem.orderId = newOrder.id)
      );
      await orderedItemsRepos.bulkCreate(orderedItemsArr, t);
      return responseObject(200, true, "registerOrder", `Order Registered`, {
        newOrder: _.omit(newOrder.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllOrders(reqUser) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier) {
        const allOrders = await ordersRepos.findAll(
          {
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
            include: [
              {
                model: orderedItems,
                attributes: {
                  exclude: ["createdAt", "updatedAt", "deletedAt"],
                },
              },
            ],
          },
          t
        );
        return responseObject(200, true, "getALlOrders", `All Orders`, {
          allOrders,
        });
      }
      const allOrders = await ordersRepos.findAll(
        {
          where: { username: reqUser.username },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          include: [
            {
              model: orderedItems,
              attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
            },
          ],
        },
        t
      );
      return responseObject(200, true, "getALlOrders", `All Orders`, {
        allOrders,
      });
    });
    return response;
  }

  async getOrderById(reqUser, id) {
    const response = sequelize.transaction(async (t) => {
      const order = await ordersRepos.findOne(
        {
          where: { id },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          include: [
            {
              model: orderedItems,
              attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
            },
          ],
        },
        t
      );
      if (!order) {
        throw new ErrorClass(
          404,
          false,
          "getOrderById",
          `Order "${id}" Not Found`
        );
      }
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier || reqUser.username === order.username) {
        return responseObject(200, true, "getOrderById", `Order "${id}"`, {
          order,
        });
      }
      throw new ErrorClass(403, false, "getOrderById", `Access Denied`);
    });
    return response;
  }

  async updateOrderById(reqUser, id, reqBody) {
    const response = await sequelize.transaction(async (t) => {
      const matchPin = await compare(reqBody.pin, reqUser.pin);
      if (!matchPin)
        throw new ErrorClass(
          403,
          false,
          "updateOrderById",
          "Incorrect Pin Number"
        );
      const order = await ordersRepos.findOne({ where: { id } }, t);
      if (!order)
        throw new ErrorClass(
          404,
          false,
          "updateOrderById",
          `Order "${id}" Not Found`
        );
      if (reqUser.username !== order.username)
        throw new ErrorClass(
          404,
          false,
          "updateOrderById",
          `Order "${order.id}" Does Not Belong to "${reqUser.username}"`
        );
      if (order.status === "processed" || order.status === "ready")
        throw new ErrorClass(
          404,
          false,
          "updateOrderById",
          `Can't Update, Order "${order.id}" is "${order.status}"`
        );
      if (order.paid)
        throw new ErrorClass(
          404,
          false,
          "updateOrderById",
          `Can't Update "paid" Order`
        );
      const oldOrderedItems = await orderedItemsRepos.findAll(
        {
          where: { orderId: order.id },
        },
        t
      );

      const oldOrderedItemsQuantity = {};
      for (let i = 0; i < oldOrderedItems.length; i++) {
        oldOrderedItemsQuantity[oldOrderedItems[i].itemName] =
          oldOrderedItems[i].quantity;
      }

      const updatedOrderedItems = reqBody.orderedItemsArr;
      for (let i = 0; i < updatedOrderedItems.length; i++) {
        const item = await itemsRepos.findOne(
          { where: { name: updatedOrderedItems[i].itemName } },
          t
        );
        if (
          item.availableQuantity <
          updatedOrderedItems[i].quantity -
            (oldOrderedItemsQuantity[item.name]
              ? oldOrderedItemsQuantity[item.name]
              : 0)
        )
          throw new ErrorClass(
            403,
            false,
            "updateOrderById",
            `${
              updatedOrderedItems[i].itemName
            } Out of Stock, Available Quantity: ${
              item.availableQuantity +
              (oldOrderedItemsQuantity[item.name]
                ? oldOrderedItemsQuantity[item.name]
                : 0)
            }`
          );
        await itemsRepos.update(
          {
            soldQuantity:
              item.soldQuantity +
              updatedOrderedItems[i].quantity -
              (oldOrderedItemsQuantity[item.name]
                ? oldOrderedItemsQuantity[item.name]
                : 0),
            availableQuantity:
              item.availableQuantity -
              updatedOrderedItems[i].quantity +
              (oldOrderedItemsQuantity[item.name]
                ? oldOrderedItemsQuantity[item.name]
                : 0),
          },
          item,
          t
        );
        updatedOrderedItems[i].total =
          item.price * updatedOrderedItems[i].quantity;

        delete oldOrderedItemsQuantity[item.name];
      }

      let total = updatedOrderedItems
        .map((updatedOrderedItem) => updatedOrderedItem.total)
        .reduce(
          (partialTotal, updatedOrderedItemTotal) =>
            partialTotal + updatedOrderedItemTotal,
          0
        );
      const userAccount = await userAccountsRepos.findOne(
        {
          where: { username: order.username },
        },
        t
      );
      if (Math.abs(userAccount.balance - total) > userAccount.creditLimit)
        throw new ErrorClass(
          403,
          false,
          "updateOrderById",
          "Order-Amount Excedes Credit Limit"
        );
      await ordersRepos.update(
        {
          total: total,
        },
        order,
        t
      );

      updatedOrderedItems.forEach((orderedItem) => {
        orderedItem.orderId = order.id;
      });

      for (let i = 0; i < updatedOrderedItems.length; i++) {
        const orderedItem = await orderedItemsRepos.findOne(
          {
            where: {
              orderId: updatedOrderedItems[i].orderId,
              itemName: updatedOrderedItems[i].itemName,
            },
          },
          t
        );
        if (orderedItem)
          await orderedItemsRepos.update(
            updatedOrderedItems[i],
            orderedItem,
            t
          );
        else await orderedItemsRepos.create(updatedOrderedItems[i], t);
      }
      const excludedOrderedItemsNames = Object.keys(oldOrderedItemsQuantity);
      for (let i = 0; i < excludedOrderedItemsNames.length; i++) {
        const excludedOrderedItem = await orderedItemsRepos.findOne({
          where: {
            orderId: order.id,
            itemName: excludedOrderedItemsNames[i],
          },
        });
        await orderedItemsRepos.delete(excludedOrderedItem, t);
      }
      return responseObject(
        200,
        true,
        "updateOrderById",
        `Order "${order.id}" Updated`,
        {
          updatedOrder: _.omit(order.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deleteOrderById(reqUser, id, reqBody) {
    const response = sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch)
        throw new ErrorClass(
          403,
          false,
          "deleteOrderById",
          `Incorrect Password`
        );
      const order = await ordersRepos.findOne(
        {
          where: {
            id,
          },
        },
        t
      );
      if (!order) {
        throw new ErrorClass(
          404,
          false,
          "deleteOrderById",
          `Order "${id}" Not Found`
        );
      }
      if (order.status === "processed" || order.status === "ready") {
        throw new ErrorClass(
          404,
          false,
          "deleteOrderById",
          `Can't Delete "${order.status}" Order`
        );
      }
      if (order.paid)
        throw new ErrorClass(
          404,
          false,
          "deleteOrderById",
          `Can't Delete "paid" Order`
        );
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier || reqUser.username === order.username) {
        const allOrderedItems = await orderedItemsRepos.findAll(
          {
            where: { orderId: order.id },
          },
          t
        );
        for (let i = 0; i < allOrderedItems.length; i++) {
          const item = await itemsRepos.findOne(
            { where: { name: allOrderedItems[i].itemName } },
            t
          );
          await itemsRepos.update(
            {
              soldQuantity: item.soldQuantity - allOrderedItems[i].quantity,
              availableQuantity:
                item.availableQuantity + allOrderedItems[i].quantity,
            },
            item,
            t
          );
        }
        await ordersRepos.delete(order, t);
        return responseObject(
          200,
          true,
          "deleteOrderById",
          `Order "${id}" Deleted`
        );
      }
      throw new ErrorClass(
        403,
        false,
        "deleteOrderById",
        `Order "${order.id}" Does Not Belong to "${reqUser.username}"`
      );
    });
    return response;
  }

  async getAllOrdersByUsername(reqUser, username) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = findRoles(reqUser.username);
      if (reqUser.username === username || isAdmin || isCashier) {
      }
      const allOrders = await ordersRepos.findAll(
        {
          where: {
            username,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllOrdersByUsername",
        `All Orders of "${username}"`,
        { allOrders }
      );
    });
    return response;
  }

  async updateOrderStatusById(reqUser, id, status) {
    const response = sequelize.transaction(async (t) => {
      const { isAdmin, isCashier } = await findRoles(reqUser.username);
      if (isAdmin || isCashier) {
        const order = await ordersRepos.findOne(
          {
            where: {
              id,
            },
          },
          t
        );
        if (!order) {
          throw new ErrorClass(
            404,
            false,
            "updateOrderStatusById",
            `Order "${id}" Not Found`
          );
        }
        await ordersRepos.update(
          {
            status,
          },
          order,
          t
        );
        if (order.status === "ready") {
          this.emitterInstance.emit("orderIsReady", {
            orderId: id,
            username: order.username,
          });
        }
        return responseObject(
          200,
          true,
          "updateOrderStatusById",
          `Order "${id}" is "${order.status}"`,
          {
            updatedOrder: _.omit(order.dataValues, [
              "createdAt",
              "updatedAt",
              "deletedAt",
            ]),
          }
        );
      }
      throw new ErrorClass(
        404,
        false,
        "updateOrderStatusById",
        "Access Denied"
      );
    });
    return response;
  }
}

module.exports = new OrdersServices();

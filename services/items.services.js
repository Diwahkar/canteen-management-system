const { sequelize } = require("../models");
const { compare } = require("../utils/hash");
const itemsRepos = require("../repos/items.repos");
const categoriesRepos = require("../repos/categories.repos");
const cloudinary = require("../utils/cloudinary");
const responseObject = require("../utils/response.object");
const ErrorClass = require("../utils/custom.error");
const _ = require("lodash");

class ItemServices {
  async registerItem(reqUser, reqBody) {
    const response = await sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, reqUser.password);
      if (!passwordMatch) {
        throw new ErrorClass(403, false, "registerItem", `Incorrect Password`);
      }
      const image = await cloudinary.uploader.upload(reqBody.image);
      reqBody.image = image.url;
      const newItem = await itemsRepos.create(reqBody, t);
      return responseObject(200, true, "registerItem", `Item Registered`, {
        newItem: _.omit(newItem.dataValues, [
          "createdAt",
          "updatedAt",
          "deletedAt",
        ]),
      });
    });
    return response;
  }

  async getAllItems() {
    const response = await await sequelize.transaction(async (t) => {
      const allItems = await itemsRepos.findAll(
        { attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] } },
        t
      );
      return responseObject(200, true, "getAllItems", "All Items", {
        allItems,
      });
    });
    return response;
  }

  async getAvailableItems() {
    const response = await sequelize.transaction(async (t) => {
      const availableItems = await itemsRepos.findAll(
        {
          where: {
            availableQuantity: {
              [Op.gte]: 1,
            },
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
        },
        t
      );
      return responseObject(200, true, "getAvailableItems", "Available Items", {
        availableItems,
      });
    });
    return response;
  }

  async getItemByName(name) {
    const response = await sequelize.transaction(async (t) => {
      const item = await itemsRepos.findOne(
        {
          where: {
            name,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!item) {
        throw new ErrorClass(
          404,
          false,
          "geItemByName",
          `Item "${name}" Not Found`
        );
      }
      return responseObject(200, true, "getItemByName", `Item "${name}"`, item);
    });
    return response;
  }

  async updateItemByName(admin, name, reqBody) {
    const response = await sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "updateItemByName",
          `Incorrect Password`
        );
      }
      const item = await itemsRepos.findOne(
        {
          where: {
            name,
          },
        },
        t
      );
      if (!item) {
        throw new ErrorClass(
          404,
          false,
          "updateItemByName",
          `Item "${name}" Not Found`
        );
      }
      if (reqBody.image) {
        const oldImage = item.image;
        const imageId = oldImage.split("/").pop().split(".")[0];
        await cloudinary.uploader.destroy(imageId);
        const image = await cloudinary.uploader.upload(reqBody.image);
        reqBody.image = image.url;
      }

      await itemsRepos.update(
        {
          name: reqBody.name || item.name,
          price:
            reqBody.price === 0 ? reqBody.price : reqBody.price || item.price,
          image: reqBody.image || item.image,
          availableQuantity:
            reqBody.availableQuantity === 0
              ? reqBody.availableQuantity
              : reqBody.availableQuantity || item.availableQuantity,
          soldQuantity:
            reqBody.soldQuantity === 0
              ? reqBody.soldQuantity
              : reqBody.soldQuantity || item.soldQuantity,
          categoryName: reqBody.categoryName || item.categoryName,
        },
        item,
        t
      );
      return responseObject(
        200,
        true,
        "updateItemByName",
        `Item "${name}" Updated`,
        {
          updatedItem: _.omit(item.dataValues, [
            "createdAt",
            "updatedAt",
            "deletedAt",
          ]),
        }
      );
    });
    return response;
  }

  async deleteItemByName(admin, name, reqBody) {
    const response = await sequelize.transaction(async (t) => {
      const passwordMatch = await compare(reqBody.password, admin.password);
      if (!passwordMatch) {
        throw new ErrorClass(
          403,
          false,
          "deleteItemByName",
          `Incorrect Password`
        );
      }
      const item = await itemsRepos.findOne(
        {
          where: { name },
        },
        t
      );
      if (!item) {
        throw new ErrorClass(
          404,
          false,
          "deleteItemByName",
          `Item "${name}" Not Found`
        );
      }
      await itemsRepos.delete(item, t);
      const itemImage = item.image;
      const imageId = itemImage.split("/").pop().split(".")[0];
      await cloudinary.uploader.destroy(imageId);
      return responseObject(
        200,
        true,
        "deleteItemByName",
        `Item "${name}" Deleted.`,
        null
      );
    });
    return response;
  }

  async getAllItemsByCategory(categoryName) {
    const response = await sequelize.transaction(async (t) => {
      const category = await categoriesRepos.findOne(
        {
          where: { name: categoryName },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      if (!category) {
        throw new ErrorClass(
          404,
          false,
          "getAllAccessesByModule",
          `Category "${categoryName}" Not Found`
        );
      }
      const allItems = await itemsRepos.findAll(
        {
          where: {
            categoryName: categoryName,
          },
          attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        },
        t
      );
      return responseObject(
        200,
        true,
        "getAllItemsByCategory",
        `Items in "${categoryName}"`,
        { allItems }
      );
    });
    return response;
  }
}

module.exports = new ItemServices();
